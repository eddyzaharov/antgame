from multiprocessing import Process
from threading import Thread
import pygame
import numpy as np
import messages
from scipy import spatial as sp
from objects_template import *
from object_behaviour import Behaviour
from lib_obj import GameItem, GameItems, Inventory
import sys
from pyglet import clock
sys.setrecursionlimit(10000)

class ObjectsState:
    Start, Pause, Run, Exit = range(4)


class ObjectArray:
    def __init__(self, smell_field, messenger,  game_item_lib, game_items_lib):
        self.messenger = messenger
        self.objects_width = None
        self.objects_height = None
        self.game_item_lib = game_item_lib
        self.game_items_lib = game_items_lib
        self.smell_field = smell_field
        self.inventory_helper = Inventory()

        self.current_objects = self.game_items_lib.generate_empty_objects()

        self.disttree = DistTree(self.current_objects)
        self.behaviour = Behaviour(self.messenger, self.disttree, self.current_objects, self.game_item_lib, self.game_items_lib)
        self.cycles_mod = 0

    def set_objects_settings(self, objects_width, objects_height, configuration=None):
        self.objects_width = objects_width
        self.objects_height = objects_height
        if configuration:
            for key in configuration:
                for item in configuration[key]:
                    obj_subtype, x, y = item
                    self.add_object(key, obj_subtype, x, y)
        self.behaviour.set_behaviour_settings(objects_width, objects_height, self.smell_field)

    def add_object(self, obj_type, obj_subtype, x, y):
        start, end = ObjectType.offset(obj_type)
        for ind in range(start, end):
            #search for empty space for object
            if self.current_objects[ind][ObjectProp.ObjType] == ObjectType.Absent:
                energy = Constants.DefaultMaxEnergy
                if obj_type == ObjectType.Ant1 or obj_type == ObjectType.Ant2:
                    if obj_subtype == ObjectSubtype.AntWarrior:
                        energy = Constants.AntWarriorMaxEnergy
                    elif obj_subtype == ObjectSubtype.AntWorker:
                        energy = Constants.AntWorkerMaxEnergy
                elif obj_type == ObjectType.Colony1 or obj_type == ObjectType.Colony2:
                    energy = Constants.ColonyMaxEnergy
                elif obj_type == ObjectType.Food and obj_subtype == ObjectSubtype.FoodCorpse:
                    energy = Constants.CorpseEnergy
                self.current_objects[ind] = self.game_item_lib.generate_new_object_with_id(ind, obj_type, obj_subtype, x, y, energy)
                return True
        return False

    def train_unit(self, player_type, unit_type):
        colony = self.game_items_lib.get_colony_by_object_type(player_type, self.current_objects)
        colony_type, = self.game_item_lib.get_props((ObjectProp.ObjType,), colony)
        team = ObjectType.get_team_by_type(colony_type)
        carrying, x, y = self.game_item_lib.get_props((ObjectProp.Carrying, ObjectProp.Xcoord, ObjectProp.Ycoord), colony)
        if unit_type == ObjectSubtype.AntWarrior:
            cost = Constants.WarriorCost
        elif unit_type == ObjectSubtype.AntWorker:
            cost = Constants.WorkerCost
        else:
            return
        if carrying >= cost:
            success = self.add_object(team, unit_type, x, y)
            if success:
                self.game_item_lib.set_props({ObjectProp.Carrying: carrying - cost}, colony)

    def get_nearest_action_units(self, player_state):
        x, y, obj_id, obj_type = self.game_item_lib.get_props((ObjectProp.Xcoord, ObjectProp.Ycoord, ObjectProp.ObjId, ObjectProp.ObjType), player_state)
        r = Constants.ActionRadius
        objids = self.disttree.query_ball_point(x, y, r, obj_id)

        nearest = {}
        for item in objids:
            if item == obj_id:
                continue
            itemtype = ObjectType.type_by_id(item)
            orient_type = InfoSmell.classificate(obj_type, itemtype)
            if orient_type in nearest:
                nearest[orient_type].append(item)
            else:
                nearest[orient_type] = [item]
        return nearest

    def king_use_item(self, player_type):
        king = self.game_items_lib.get_king_by_object_type(player_type, self.current_objects)
        #inv, act_item = self.game_item_lib.get_props((ObjectProp.Inventory, ObjectProp.ActiveItem), king)
        form_inv, act_item = self.inventory_helper.get_inventory_formatted(king)
        if act_item == Inventory.NoObject:
            return
        success = self.inventory_helper.add_remove_item(act_item, king, add=False)
        if not success:
            return

        if act_item == Inventory.TeleportInd:
            self.game_item_lib.set_props({ObjectProp.State: ObjectState.KingTeleporting,
                                          ObjectProp.StateTime: np.int32(0),
                                          ObjectProp.DirVelocity: np.int32(0)}, king)
        elif act_item == Inventory.BombInd:
            self.game_item_lib.set_props({ObjectProp.State: ObjectState.KingBombing,
                                          ObjectProp.StateTime: np.int32(0)}, king)

        elif act_item == Inventory.HealerInd:
            self.game_item_lib.set_props({ObjectProp.State: ObjectState.KingHealing,
                                          ObjectProp.StateTime: np.int32(0)}, king)

        elif act_item == Inventory.FreezeInd:
            self.game_item_lib.set_props({ObjectProp.State: ObjectState.KingFreezing,
                                          ObjectProp.StateTime: np.int32(0)}, king)

    def update_items(self):
        if self.current_objects is None:
            return
        if self.cycles_mod >= 15:
            self.cycles_mod = 0
            self.disttree.update_kdtree()
        self.behaviour.update_all_state()
        self.cycles_mod += 1

    def get_objects(self, link_only=False):
        if link_only:
            return self.current_objects
        return np.copy(self.current_objects)


class DistTree:
    def __init__(self, current_state, scaling_factor=1000):
        self.kdtree = None
        self.scaling_factor = scaling_factor
        self.stored_points = None
        self.current_state = current_state
        self.scaledcoords = None
        self.nearest_ids = None
        self.nearest = None

    def update_kdtree(self):
        self.scaledcoords = self.current_state[np.nonzero(self.current_state[:, 1]), :][0,:][:, [ObjectProp.ObjId, ObjectProp.Xcoord, ObjectProp.Ycoord]]
        if not self.scaledcoords.size:
            self.kdtree = None
            return
        self.scaledcoords[:, 1] *= self.scaling_factor
        self.scaledcoords[:, 2] *= self.scaling_factor
        self.stored_points = self.scaledcoords
        self.kdtree = sp.cKDTree(self.scaledcoords)

    def query_ball_point(self, x, y, r, obj_id):
        if self.kdtree:
            self.nearest_ids = self.kdtree.query_ball_point(np.array([obj_id, x * self.scaling_factor, y * self.scaling_factor]), r * self.scaling_factor)
            self.nearest = self.stored_points[self.nearest_ids][:, 0]
            return self.nearest
        return []


class SmellField:
    def __init__(self, field_layers=5, field_width=None, field_height=None, objects_width=None, objects_height=None):
        self.field = None
        self.field_width = field_width
        self.field_height = field_height
        self.field_layers = field_layers
        self.objects_width = objects_width
        self.objects_height = objects_height
        self.x_ratio = None
        self.y_ratio = None #+1 here because of cell is [start, end) if coordinate is at the edge index is too big
        self.res_x = None
        self.res_y = None
        self.extremum = None
        self.field_value = None
        self.x_lookup = None
        self.y_lookup = None


    def set_field_settings(self, field_width, field_height, objects_width, objects_height):
        self.field_width = field_width
        self.field_height = field_height
        self.objects_width = objects_width
        self.objects_height = objects_height
        self.field = np.zeros((field_width, field_height, self.field_layers), dtype=np.int32)
        self.x_ratio = (objects_width + 1) / self.field_width
        self.y_ratio = (objects_height + 1) / self.field_height #+1 here because of cell is [start, end) if coordinate is at the edge index is too big

    def get_shape(self):
        return self.field.shape

    def update_field(self, field):
        del self.field
        self.field = field

    def field_indexes(self, x_coord, y_coord):
        return np.int32(x_coord // self.x_ratio), np.int32(y_coord // self.y_ratio)

    def get_gradient(self, x_ind, y_ind, field_smell, towards=True):
        self.res_x, self.res_y = np.int32(0), np.int32(0)
        self.extremum = 0 if towards else 1000
        for xind in (-1, 0, 1):
            for yind in (-1, 0, 1):
                self.x_lookup = x_ind + xind
                self.y_lookup = y_ind + yind
                if self.x_lookup < 0 or self.x_lookup > self.field_width - 1 or self.y_lookup < 0 or self.y_lookup > self.field_height - 1:
                    continue
                self.field_value = sum((self.field[self.x_lookup, self.y_lookup, f_ind] for f_ind in field_smell)) if type(field_smell) == tuple else self.field[self.x_lookup, self.y_lookup, field_smell]
                if towards:
                    if self.field_value > self.extremum:
                        self.res_x = xind
                        self.res_y = yind
                        self.extremum = self.field_value
                else:
                    if self.field_value < self.extremum:
                        self.res_x = xind
                        self.res_y = yind
                        self.extremum = self.field_value
        return self.res_x, self.res_y


class Objects(Process):
    def __init__(self, messenger, screen_width, screen_height,  framerate=30, verbose_fps=False):
        super(Objects, self).__init__()
        self.objects_state = ObjectsState.Start
        self.messenger = messenger
        self.objects_width = None
        self.objects_height = None
        self.field_width = None
        self.field_height = None
        self.field_layers = FieldSmell.Total
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.configuration = None
        self.framerate = framerate
        self.verbose_fps = verbose_fps
        self.clock = pygame.time.Clock()
        self.cycles = 0
        self.framerate = framerate
        self.game_item_lib = GameItem(screen_width, screen_height)
        self.game_items_lib = GameItems()
        self.smell_field = SmellField()
        self.objects = ObjectArray(self.smell_field, self.messenger, self.game_item_lib, self.game_items_lib)
        self.player_action = PlayerAction(self.messenger, self.objects, self.game_item_lib, self.game_items_lib)
        self.functions = {messages.Objects.Quit: self.quit,
                          messages.Objects.UpdateField: self.smell_field.update_field,
                          messages.Objects.AddObject: self.objects.add_object,
                          messages.Objects.KingSetDestination: self.player_action.set_destination_point,
                          messages.Objects.KingSetDirection: self.player_action.set_direction,
                          messages.Objects.KingChangeItem: self.player_action.change_item,
                          messages.Objects.KingSwitchSmell: self.player_action.switch_smell,
                          messages.Objects.Pause: self.pause_simulation,
                          messages.Objects.Run: self.run_simulation,
                          messages.Objects.UpdateGameSettings: self.update_game_settings,
                          messages.Objects.TrainUnit: self.objects.train_unit,
                          messages.Objects.BuyItem: self.player_action.buy_item,
                          messages.Objects.UseItem: self.objects.king_use_item}
        self.objects_state = ObjectsState.Pause

    def quit(self):
        self.objects_state = ObjectsState.Exit

    def pause_simulation(self):
        self.objects_state = ObjectsState.Pause

    def run_simulation(self):
        self.objects_state = ObjectsState.Run

    def update_game_settings(self, objects_width, objects_height, field_width, field_height, configuration):
        self.objects_width = objects_width
        self.objects_height = objects_height
        self.field_width = field_width
        self.field_height = field_height
        self.configuration = configuration
        self.smell_field.set_field_settings(field_width, field_height, objects_width, objects_height)
        self.objects.set_objects_settings(objects_width, objects_height, configuration)
        self.smell_field.set_field_settings(field_width, field_height, objects_width, objects_height)
        self.game_item_lib.set_game_settings(objects_width, objects_height, field_width, field_height)

    def run(self):
        while self.objects_state != ObjectsState.Exit:
            while True:
                data = self.messenger.get_message(messages.Objects)
                if not data:
                    break
                self.functions[data['func']](**data['args']) if 'args' in data else self.functions[data['func']]()
            if self.objects_state == ObjectsState.Run:
                self.objects.update_items()
                self.messenger.game_update_objects(self.objects.get_objects())
                self.messenger.controls_update_objects(self.objects.get_objects())
                #self.messenger.renderer_update_objects(self.objects.get_objects())
                #self.messenger.controls_update_objects(self.objects.get_objects())
            elapsed = self.clock.tick(self.framerate)
            if self.cycles % 30 == 0 and self.verbose_fps:
                print("elapsed: {}".format(elapsed))
            self.cycles += 1


class PlayerAction:
    def __init__(self, messenger, objects_array, game_item_lib, game_items_lib):
        self.game_item_lib = game_item_lib
        self.game_items_lib = game_items_lib
        self.messenger = messenger
        self.objects = objects_array.get_objects(link_only=True)
        self.inventory_helper = Inventory()

    def set_destination_point(self, player_type, x, y):
        king = self.game_items_lib.get_king_by_object_type(player_type, self.objects)
        self.game_item_lib.set_props({ObjectProp.DestinationX: np.int32(x), ObjectProp.DestinationY: np.int32(y)}, king)

    def set_direction(self, player_type, x, y):
        king = self.game_items_lib.get_king_by_object_type(player_type, self.objects)

        if x == 0 and y == 0:
            self.game_item_lib.set_props({ObjectProp.DirVelocity: np.int(0)}, king)
        else:
            self.game_item_lib.set_props({ObjectProp.Xdirection: np.int32(x),
                                          ObjectProp.Ydirection: np.int32(y),
                                          ObjectProp.DirVelocity: Constants.KingMaxSpeed}, king)

    def change_item(self, player_type, next_item=True):
        king = self.game_items_lib.get_king_by_object_type(player_type, self.objects)
        self.inventory_helper.change_active_item(king, next_item)

    def buy_item(self,  player_type, item_type):
        king = self.game_items_lib.get_king_by_object_type(player_type, self.objects)
        inv, act_item = self.game_item_lib.get_props((ObjectProp.Inventory, ObjectProp.ActiveItem), king)
        colony = self.game_items_lib.get_colony_by_object_type(player_type, self.objects)
        carrying, = self.game_item_lib.get_props((ObjectProp.Carrying,), colony)
        cost = Inventory.Cost[item_type]
        if carrying >= cost:
            #success = self.inventory_helper.add_remove_item(item_type, king, add=True)
            if self.inventory_helper.add_remove_item(item_type, king, add=True):
                self.game_item_lib.set_props({ObjectProp.Carrying: carrying-cost}, colony)
        #inv, act_item = self.game_item_lib.get_props((ObjectProp.Inventory, ObjectProp.ActiveItem), king)


    def switch_smell(self, player_type, next_smell=True):
        king = self.game_items_lib.get_king_by_object_type(player_type, self.objects)
        infosmell, = self.game_item_lib.get_props((ObjectProp.InfoSmellType,), king)
        enemy = InfoSmell.Ant2 if player_type == ObjectType.King1 else InfoSmell.Ant1
        home = InfoSmell.Colony1 if player_type == ObjectType.King1 else InfoSmell.Colony2
        if next_smell:
            next_bind = {InfoSmell.NoSmell: enemy,
                         InfoSmell.Food: InfoSmell.NoSmell,
                         home: InfoSmell.Food,
                         enemy: home}
        else:
            next_bind = {InfoSmell.NoSmell: InfoSmell.Food,
                         InfoSmell.Food: home,
                         home: enemy,
                         enemy: InfoSmell.NoSmell}

        new_type = next_bind[infosmell]
        new_value = InfoSmell.start_value() if new_type != InfoSmell.NoSmell else np.int32(0)
        self.game_item_lib.set_props({ObjectProp.InfoSmellType: new_type,
                                      ObjectProp.InfoSmellValue: new_value,
                                      ObjectProp.InfoSmellCycles: np.int32(0)}, king)
