import numpy as np

class ObjectType:
    Absent = np.int32(0)
    Ant1 = np.int32(1)
    Ant2 = np.int32(2)
    King1 = np.int32(3)
    King2 = np.int32(4)
    Colony1 = np.int32(5)
    Colony2 = np.int32(6)
    Food = np.int32(7)
    Total = np.int32(8)
    offsets = {
        King1: (np.int32(0), np.int32(1)),
        Ant1: (np.int32(1), np.int32(100)),
        King2: (np.int32(100), np.int32(101)),
        Ant2: (np.int32(101), np.int32(200)),
        Colony1: (np.int32(200), np.int32(201)),
        Colony2: (np.int32(201), np.int32(202)),
        Food: (np.int32(202), np.int32(256))}

    @classmethod
    def offset(cls, obj_type):
        return cls.offsets[obj_type]

    @classmethod
    def object_layers(cls):
        # Food, Ant, King, Colony
        return [range(202, 256), range(1, 100), range(101, 200), range(0, 1), range(100, 101), range(200,201), range(201, 202)]

    @classmethod
    def get_colony_id_by_type(cls, obj_type):
        if obj_type == cls.King1 or obj_type == cls.Ant1 or obj_type == cls.Colony1:
            return np.int32(200)
        elif obj_type == cls.King2 or obj_type == cls.Ant2 or obj_type == cls.Colony2:
            return np.int32(201)

    @classmethod
    def get_king_id_by_type(cls, obj_type):
        if obj_type == cls.King1 or obj_type == cls.Ant1 or obj_type == cls.Colony1:
            return np.int32(0)
        elif obj_type == cls.King2 or obj_type == cls.Ant2 or obj_type == cls.Colony2:
            return np.int32(100)

    @classmethod
    def get_team_by_type(cls, obj_type):
        if obj_type in (cls.King1, cls.Ant1, cls.Colony1):
            return cls.Ant1
        elif obj_type in (cls.King2, cls.Ant2, cls.Colony2):
            return cls.Ant2

    @classmethod
    def type_by_id(cls, obj_id):
        value = cls.Absent
        if obj_id == np.int32(0):
            value = cls.King1
        elif 1 <= obj_id < 100:
            value = cls.Ant1
        elif obj_id == np.int32(100):
            value = cls.King2
        elif 101 <= obj_id < 200:
            value = cls.Ant2
        elif obj_id == 200:
            value = cls.Colony1
        elif obj_id == 201:
            value = cls.Colony2
        elif 202 <= obj_id < 256:
            value = cls.Food
        return value


class FieldSmell:
    Food = np.int32(0)
    Ant1 = np.int32(1)
    Ant2 = np.int32(2)
    Colony1 = np.int32(3)
    Colony2 = np.int32(4)
    Total = np.int32(5)
    smell_binding = {ObjectType.Ant1: Ant1,
                     ObjectType.King1: Ant1,
                     ObjectType.Ant2: Ant2,
                     ObjectType.King2: Ant2,
                     ObjectType.Colony1: Colony1,
                     ObjectType.Colony2: Colony2,
                     ObjectType.Food: Food}

    @classmethod
    def get_smell(cls, obj_type):
        return cls.smell_binding[obj_type]

class ObjectSubtype:
    AntWorker = np.int32(0)
    AntWarrior = np.int32(1)
    FoodApple = np.int32(2)
    FoodCorpse = np.int32(3)
    ColonySimple = np.int32(4)
    PlayerSimple = np.int32(5)
    Total = np.int32(6)


class ObjectState:
    ObjectDestroyed = np.int32(0)
    ObjectIdle = np.int32(1)
    AntSearchingFood = np.int32(2)
    AntGoingHome = np.int32(3)
    AntSearchingEnemy = np.int32(4)
    AntAttacking = np.int32(5)
    AntGathering = np.int32(6)
    AntRunaway = np.int32(7)
    AntTeleported = np.int32(8)
    AntBombed = np.int32(9)
    AntFreezed = np.int32(10)
    KingFree = np.int32(11)
    KingAutoAttack = np.int32(12)
    KingGatherFood = np.int32(13)
    KingTeleporting = np.int32(14)
    KingBombing = np.int32(15)
    KingHealing = np.int32(16)
    KingFreezing= np.int32(17)
    Healing = np.int32(18)
    Total = np.int32(19)


class InfoSmell:
    NoSmell = np.int32(0)
    Food = np.int32(1)
    Ant1 = np.int32(2)
    Ant2 = np.int32(3)
    Colony1 = np.int32(4)
    Colony2 = np.int32(5)

    @classmethod
    def convert_to_field_smell_write(cls, smell):
        return {cls.Food: (FieldSmell.Food,),
                cls.Ant1: (FieldSmell.Ant1,),
                cls.Ant2: (FieldSmell.Ant2,),
                cls.Colony1: (FieldSmell.Colony1,),
                cls.Colony2: (FieldSmell.Colony2,)}[smell]

    @classmethod
    def get_smell_by_obj_type(cls, smell):
        return {ObjectType.Absent: cls.NoSmell,
                ObjectType.Food: cls.Food,
                ObjectType.Ant1: cls.Ant1,
                ObjectType.King1: cls.Ant1,
                ObjectType.Ant2: cls.Ant2,
                ObjectType.King2: cls.Ant2,
                ObjectType.Colony1: cls.Colony1,
                ObjectType.Colony2: cls.Colony2}[smell]

    @classmethod
    def decay_rate(cls):
        return np.int32(15)

    @classmethod
    def decay_value(cls):
        return np.int32(10)

    @classmethod
    def start_value(cls):
        return np.int32(100)

    @classmethod
    def convert_to_field_read(cls, obj_type, orient_smell):
        if obj_type == ObjectType.Ant1:
            return {cls.Food: FieldSmell.Food,
                    cls.Colony1: FieldSmell.Colony1,
                    cls.Ant2: (FieldSmell.Ant2, FieldSmell.Colony2)}[orient_smell]
        else:
            return {cls.Food: FieldSmell.Food,
                    cls.Colony2: FieldSmell.Colony2,
                    cls.Ant1: (FieldSmell.Ant1, FieldSmell.Colony1)}[orient_smell]

    @classmethod
    def classificate(cls, obj_type, itemtype):
        if obj_type == ObjectType.Ant1 or obj_type == ObjectType.King1:
            binding = {ObjectType.Ant1: cls.Ant1,
                       ObjectType.Colony1: cls.Colony1,
                       ObjectType.King1: cls.Ant1,
                       ObjectType.Ant2: cls.Ant2,
                       ObjectType.Colony2: cls.Ant2,
                       ObjectType.King2: cls.Ant2,
                       ObjectType.Food: cls.Food}

        else:
            binding = {ObjectType.Ant1: cls.Ant1,
                       ObjectType.Colony1: cls.Ant1,
                       ObjectType.King1: cls.Ant1,
                       ObjectType.Ant2: cls.Ant2,
                       ObjectType.Colony2: cls.Colony2,
                       ObjectType.King2: cls.Ant2,
                       ObjectType.Food: cls.Food}
        return binding[itemtype]

class ObjectProp:
    ObjId = np.int32(0)
    ObjType = np.int32(1)
    ObjSubtype = np.int32(2)
    Xcoord = np.int32(3)
    Ycoord = np.int32(4)
    DirVelocity = np.int32(5)
    Xdirection = np.int32(6)
    Ydirection = np.int32(7)
    State = np.int32(8)
    StateTime = np.int32(9)
    Energy = np.int32(10)
    Carrying = np.int32(11)
    InfoSmellType = np.int32(12)
    InfoSmellValue = np.int32(13)
    InfoSmellCycles = np.int32(14)
    CyclesLiving = np.int32(15)
    DestinationX = np.int32(16)
    DestinationY = np.int32(17)
    Inventory = np.int32(18)
    ActiveItem = np.int32(19)
    Total = np.int32(20)


class Constants:
    AntMaxSpeed = np.int32(350)
    AntCanCarry = np.int32(1)
    AntHitPower = np.int32(3000)
    KingHitPower = np.int32(5000)
    BombHitPower = np.int32(3000)
    AntFoodHit = np.int32(200)
    AntWorkerMaxEnergy = np.int32(10000)
    AntWarriorMaxEnergy = np.int32(20000)
    ColonyMaxEnergy = np.int32(80000)
    CorpseEnergy = np.int32(800)
    DefaultMaxEnergy = np.int32(10000)
    KingMaxHealth = np.int32(20000)
    KingMaxSpeed = np.int32(400)
    ActionRadius = np.int32(25000)
    DissolveCycles = np.int32(15)
    TeleportCooldownCycles = np.int32(60)
    FreezeLengthCycles = np.int32(150)
    CastLengthCycles = np.int32(30)
    BombedCooldown = np.int32(30)
    AttackCooldown = np.int32(10)
    HealCooldown = np.int32(30)
    NearestRadius = np.int32(6500)
    ChangeDirCooldown = np.int32(45)
    BorderAvoidTreshold = np.int32(1500)
    PlayerPositioningTreshold = np.int32(500)
    CountBuyableItems = np.int32(6)
    FieldMaxValue = np.int32(1000)
    WarriorCost = np.int32(15)
    WorkerCost = np.int32(10)
    ObjArrayTotal = np.int32(256)

class Teams:
    Team_A, Team_B = range(0, 2)