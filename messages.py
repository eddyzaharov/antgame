from multiprocessing import Queue
from objects_template import *
import time

class Game:
    Quit, UpdateField, UpdateObjects, RunNewGame = range(4)
class GuiControls:
    StopGui, StartGame, UpdateField, UpdateObjects, HandleMouse, HandleKey = range(6)
class GuiScreen:
    TerminateScreen, StartNewGame, UpdateRenderedSurface = range(3)
class Renderer:
    UpdateField, TerminateRenderer, UpdateObjects, Start, Pause, SwitchShowField = range(6)
class Field:
    TerminateSimulation, SetNewItemField, PauseSimulation, RunSimulation, SetFieldSettings = range(5)
class Objects:
    Quit, UpdateField, AddObject, KingSetDestination, KingSetDirection, KingSwitchSmell, \
    KingChangeItem, Pause, Run, UpdateGameSettings, TrainUnit, BuyItem, UseItem = range(13)

class Messenger:
    def __init__(self):
        self.gui_screen_queue = Queue()
        self.game_queue = Queue()
        self.field_queue = Queue()
        self.objects_queue = Queue()
        self.gui_controls_queue = Queue()
        self.renderer_queue = Queue()
        self.writable = True
        self.binding = {GuiScreen:  self.gui_screen_queue,
                        Game: self.game_queue,
                        Field: self.field_queue,
                        Objects: self.objects_queue,
                        GuiControls: self.gui_controls_queue,
                        Renderer: self.renderer_queue}

    def send_message(self, queue, func, args=None):
        if not self.writable:
            return
        mess = {'func': func}
        if args:
            mess['args'] = args
        try:
            queue.put(mess)
        except Exception as e:
            print("send excetion: {}".format(e))
            pass

    def get_message(self, obj_type):
        try:
            data = self.binding[obj_type].get(False)
            return data
        except Exception as e:
            return None

    def game_quit(self):
        self.send_message(self.game_queue, Game.Quit)

    def game_update_field(self, field_copy):
        self.send_message(self.game_queue, Game.UpdateField, {'field_copy': field_copy})

    def game_update_objects(self, objects_copy):
        self.send_message(self.game_queue, Game.UpdateObjects, {'objects_copy': objects_copy})

    def game_run_new_game(self):
        self.send_message(self.game_queue, Game.RunNewGame)

    def objects_update_field(self, field):
        self.send_message(self.objects_queue, Objects.UpdateField, {'field': field})

    def objects_add_object(self, obj_type, obj_subtype, x, y):
        self.send_message(self.objects_queue, Objects.AddObject, {'obj_type': obj_type, 'obj_subtype': obj_subtype, 'x': x, 'y': y})

    def objects_quit(self):
        self.send_message(self.objects_queue, Objects.Quit)

    def objects_king_set_destination(self, player_type, x, y):
        if player_type in (ObjectType.King1, ObjectType.King2):
            self.send_message(self.objects_queue, Objects.KingSetDestination, {'player_type': player_type, 'x': x, 'y': y})

    def objects_king_set_direction(self, player_type, x, y):
        if player_type in (ObjectType.King1, ObjectType.King2):
            self.send_message(self.objects_queue, Objects.KingSetDirection, {'player_type': player_type, 'x': x, 'y': y})

    def objects_king_change_item(self, player_type, next_item):
        if player_type in (ObjectType.King1, ObjectType.King2):
            self.send_message(self.objects_queue, Objects.KingChangeItem, {'player_type': player_type, 'next_item': next_item})

    def objects_king_switch_smell(self, player_type, next_smell):
        if player_type in (ObjectType.King1, ObjectType.King2):
            self.send_message(self.objects_queue, Objects.KingSwitchSmell, {'player_type': player_type, 'next_smell': next_smell})

    def objects_king_train_unit(self, player_type, unit_type):
        if player_type in (ObjectType.King1, ObjectType.King2) and unit_type in (ObjectSubtype.AntWorker, ObjectSubtype.AntWarrior):
            self.send_message(self.objects_queue, Objects.TrainUnit, {'player_type': player_type, 'unit_type': unit_type})

    def objects_king_buy_item(self, player_type, item_type):
        if player_type in (ObjectType.King1, ObjectType.King2):
            self.send_message(self.objects_queue, Objects.BuyItem, {'player_type': player_type, 'item_type': item_type})

    def objects_king_use_item(self, player_type):
        if player_type in (ObjectType.King1, ObjectType.King2):
            self.send_message(self.objects_queue, Objects.UseItem, {'player_type': player_type})

    def objects_run_simulation(self):
        self.send_message(self.objects_queue, Objects.Run)

    def objects_pause_simulation(self):
        self.send_message(self.objects_queue, Objects.Pause)

    def objects_set_game_settings(self, objects_width, objects_height, field_width, field_height, configuration):
        self.send_message(self.objects_queue, Objects.UpdateGameSettings, {'objects_width': objects_width,
                                                                           'objects_height': objects_height,
                                                                           'field_width': field_width,
                                                                           'field_height': field_height,
                                                                           'configuration': configuration})

    def field_terminate_simulation(self):
        self.send_message(self.field_queue, Field.TerminateSimulation)

    def field_set_new_items_field(self, items_field):
        self.send_message(self.field_queue, Field.SetNewItemField, {'field': items_field})

    def field_run_simulation(self):
        self.send_message(self.field_queue, Field.RunSimulation)

    def field_pause_simulation(self):
        self.send_message(self.field_queue, Field.PauseSimulation)

    def field_set_field_settings(self, field_width, field_height, configuration):
        self.send_message(self.field_queue, Field.SetFieldSettings, {'field_width': field_width,
                                                                     'field_height': field_height,
                                                                     'configuration': configuration})

    def screen_terminate(self):
        self.send_message(self.gui_screen_queue, GuiScreen.TerminateScreen)

    def screen_start_new_game(self):
        self.send_message(self.gui_screen_queue, GuiScreen.StartNewGame)

    def screen_update_rendered_surface(self, surface_string):
        self.send_message(self.gui_screen_queue, GuiScreen.UpdateRenderedSurface, {'surface_string': surface_string})

    def renderer_start(self):
        self.send_message(self.renderer_queue, Renderer.Start)

    def renderer_pause(self):
        self.send_message(self.renderer_queue, Renderer.Pause)

    def renderer_update_objects(self, objects):
        self.send_message(self.renderer_queue, Renderer.UpdateObjects, {'objects': objects})

    def renderer_update_field(self, field):
        self.send_message(self.renderer_queue, Renderer.UpdateField, {'field': field})

    def renderer_terminate(self):
        self.send_message(self.renderer_queue, Renderer.TerminateRenderer)

    def renderer_switch_field(self):
        self.send_message(self.renderer_queue, Renderer.SwitchShowField)

    def controls_update_field(self, field):
        self.send_message(self.gui_controls_queue, GuiControls.UpdateField, {'field': field})

    def controls_update_objects(self, objects):
        self.send_message(self.gui_controls_queue, GuiControls.UpdateObjects, {'objects': objects})

    def controls_handle_key(self, pushed, key):
        self.send_message(self.gui_controls_queue, GuiControls.HandleKey, {'pushed': pushed, 'key': key})

    def controls_handle_mouse(self, button, x, y):
        self.send_message(self.gui_controls_queue, GuiControls.HandleMouse, {"button": button, 'x': x, 'y': y})

    def controls_terminate(self):
        self.send_message(self.gui_controls_queue, GuiControls.StopGui)

    def controls_start_game(self, objects_width, objects_height, field_width, field_height):
        self.send_message(self.gui_controls_queue, GuiControls.StartGame, {'objects_width': objects_width, 'objects_height': objects_height,
                                                                           'field_width': field_width, 'field_height': field_height})

    def shutdown(self):
        print('terminate field')
        self.field_terminate_simulation()
        print("terminate objects")
        self.objects_quit()
        print("terminate controls")
        self.controls_terminate()
        print("terminate_renderer")
        self.renderer_terminate()
        print("terminate screen")
        self.screen_terminate()

        self.writable = False
        #make sure threads ended
        for t in range(0, 2):
            print("waiting for queues: {}".format(t))
            time.sleep(1)

        for key in self.binding:
            while True:
                data = self.get_message(key)
                if not data:
                    break
        print("all data is read. write is locked")

        #for key in self.binding:
        #    self.binding[key].close()
        #for key in self.binding:
        #    self.binding[key].cancel_join_thread()