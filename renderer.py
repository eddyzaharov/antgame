from multiprocessing import Process
from threading import Thread
import numpy as np
import pygame
import messages
from objects_template import *
from resource_manager import ResourceManager
from lib_obj import GameItem, GameItems, Inventory
import pyglet

class RendererState:
    Start, Pause, Game, Menu, Exit = range(5)


class Renderer:
    def __init__(self, screen_width, screen_height, resource_manager=None):
        self.renderer_state = RendererState.Start
        self.resource_manager = resource_manager
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.field_width = None
        self.field_height = None
        self.field_layers = FieldSmell.Total
        self.field_copy = None
        self.objects_copy = None
        self.objects_width = None
        self.objects_height = None
        self.current_object = None

        self.game_item_lib = GameItem(screen_width, screen_height)
        self.game_items_lib = GameItems()

        self.batch = pyglet.graphics.Batch()

        self.objects_sprites = None
        self.gui_sprites = None
        self.game_panels_sprites = None
        self.background_sprites = None
        self.all_sprites = None
        self.objects_sprites = None
        self.init_sprites()
        self.renderer_state = RendererState.Menu

    def init_sprites(self):
        self.all_sprites = []
        self.gui_sprites = []
        self.game_panels_sprites = []
        self.background_sprites = []
        self.objects_sprites = []
        for index in range(0, Constants.ObjArrayTotal):
            object_type = ObjectType.type_by_id(index)
            if object_type in (ObjectType.Ant1, ObjectType.Ant2):
                new_obj_sprite = AntRepr(self.game_item_lib, self.resource_manager, batch=self.batch)
            elif object_type in (ObjectType.King1, ObjectType.King2):
                new_obj_sprite = KingRepr(self.game_item_lib, self.resource_manager, batch=self.batch)
            elif object_type in (ObjectType.Colony1, ObjectType.Colony2):
                new_obj_sprite = ColonyRepr(self.game_item_lib, self.game_items_lib, self.resource_manager, batch=self.batch)
            elif object_type == ObjectType.Food:
                new_obj_sprite = FoodRepr(self.game_item_lib, self.resource_manager, batch=self.batch)
            self.objects_sprites.append(new_obj_sprite)
            self.all_sprites.append(new_obj_sprite)
        for colony_type in (ObjectType.Colony1, ObjectType.Colony2):
            panel = ColonyPanel(colony_type, self.game_item_lib, self.game_items_lib, self.resource_manager, self.batch, self.screen_width, self.screen_height)
            self.game_panels_sprites.append(panel)
            self.all_sprites.append(panel)

        backround = Background(self.game_item_lib, self.resource_manager, self.batch, self.screen_width, self.screen_height)
        self.background_sprites.append(backround)
        self.all_sprites.append(backround)

        main_menu = MainMenu(self.game_item_lib, self.resource_manager, self.batch, self.screen_width, self.screen_height)
        self.gui_sprites.append(main_menu)
        self.all_sprites.append(main_menu)


    def set_game_settings(self, objects_width, objects_height, field_width, field_height):
        self.game_item_lib.set_game_settings(objects_width, objects_height, field_width, field_height)
        self.objects_width = objects_width
        self.objects_height = objects_height
        self.field_width = field_width
        self.field_height = field_height

    def start_new_game(self):
        self.renderer_state = RendererState.Game
        self.gui_sprites[0].set_visible(False)
        self.background_sprites[0].set_visible(True)
        self.game_item_lib.set_game_settings(self.objects_width, self.objects_height, self.field_width, self.field_height)

    def update_objects(self, objects):
        del self.objects_copy
        self.objects_copy = objects

    def update_field(self, field):
        del self.field_copy
        self.field_copy = field

    def update_graphics(self):
        if self.renderer_state == RendererState.Game:
            if self.objects_copy is None:
                return
            for index in range(0, len(self.objects_sprites)):
                self.current_object = self.objects_copy[index]
                self.objects_sprites[index].update_self(self.current_object, self.objects_copy)
            for panel in self.game_panels_sprites:
                panel.update_self(self.objects_copy)
        elif self.renderer_state == RendererState.Menu:
            for gui_item in self.gui_sprites:
                gui_item.update_self()


class AntRepr(pyglet.sprite.Sprite):
    def __init__(self, game_item_lib, resource_manager, batch):
        self.game_item_lib = game_item_lib
        self.resource_manager = resource_manager
        image = self.resource_manager.get_frame(obj_type=ObjectType.Ant1, obj_subtype=ObjectSubtype.AntWorker, state=ObjectState.ObjectIdle, carrying_food=False)
        group = pyglet.graphics.OrderedGroup(2)
        self.team = None
        self.upd_x, self.upd_y = None, None
        super(AntRepr, self).__init__(img=image, batch=batch, group=group)
        self.visible = False
        self.obj_id, self.obj_subtype, self.state, self.carrying, self.dir_x, self.dir_y = None, None, None, None, None, None


    def set_visible(self, visible=False):
        self.visible = visible

    def update_self(self, object_state, objects_state=None):
        self.obj_id, self.obj_subtype, self.state, self.carrying, self.dir_x, self.dir_y = self.game_item_lib.get_props((ObjectProp.ObjId, ObjectProp.ObjSubtype,
                                                                                                                         ObjectProp.State, ObjectProp.Carrying, ObjectProp.Xdirection,
                                                                                                                         ObjectProp.Ydirection), object_state)
        if self.game_item_lib.is_absent(object_state):
            self.visible = False
        else:
            self.team = ObjectType.type_by_id(self.obj_id)
            self.image = self.resource_manager.get_frame(obj_type=self.team, obj_subtype=self.obj_subtype, state=self.state, carrying_food=self.carrying)
            self.x, self.y = self.game_item_lib.get_draw_coords(object_state)
            self.update(x=self.upd_x, y=self.upd_y)
            self.visible = True



class KingRepr(pyglet.sprite.Sprite):
    def __init__(self, game_item_lib, resource_manager, batch):
        self.game_item_lib = game_item_lib
        self.resource_manager = resource_manager
        image = self.resource_manager.get_frame(obj_type=ObjectType.King1, state=ObjectState.ObjectIdle, carrying_food=0)
        group = pyglet.graphics.OrderedGroup(3)
        self.team = None
        self.upd_x, self.upd_y = None, None
        super(KingRepr, self).__init__(img=image, batch=batch, group=group)
        self.inventory_item = pyglet.sprite.Sprite(img=resource_manager.get_inventory_object(Inventory.NoObject), group=group, batch=batch)
        self.inventory_item.visible = False

        self.item_indicator = pyglet.sprite.Sprite(img=resource_manager.get_items_number(0), group=group, batch=batch)
        self.item_indicator.visible = False

        self.smell = pyglet.sprite.Sprite(img=resource_manager.get_smell_indicator(InfoSmell.NoSmell), group=group, batch=batch)
        self.smell.visible = False
        self.obj_id, self.state, self.current_smell, self.active_item = None, None, None, None
        self.inventory, self.carrying, self.dir_x, self.dir_y = None, None, None, None
        self.inv_item_count = None

        #self.smell_label = pyglet.text.Label('',
        #                                     font_name='Helvetica',
        #                                     font_size=self.font_size,
        #                                     color=(0,0,0,255),
        #                                     x=0, y=0,
        #                                     anchor_x='center',  anchor_y='center',
        #                                     batch=batch,
        #                                     group=group)
        #self.item_bind = {Inventory.NoObject: '', Inventory.TeleportInd: 'Teleport', Inventory.BombInd: 'Bomb', Inventory.HealerInd: 'Healer', Inventory.FreezeInd: 'Freezer'}
        #self.smell_bind = {ObjectType.King1: {InfoSmell.NoSmell: '', InfoSmell.Ant2: 'Enemy', InfoSmell.Colony1: 'Home', InfoSmell.Food: 'Food'},
        #                   ObjectType.King2: {InfoSmell.NoSmell: '', InfoSmell.Ant1: 'Enemy',  InfoSmell.Colony2: 'Home', InfoSmell.Food: 'Food'}}

    def set_visible(self, visible=False):
        self.visible = visible

    def update_self(self, object_state, objects_state=None):
        self.obj_id, self.state, self.current_smell, self.active_item, self.inventory,\
        self.carrying, self.dir_x, self.dir_y = self.game_item_lib.get_props((ObjectProp.ObjId, ObjectProp.State, ObjectProp.InfoSmellType,
                                                               ObjectProp.ActiveItem, ObjectProp.Inventory,
                                                               ObjectProp.Carrying, ObjectProp.Xdirection, ObjectProp.Ydirection), object_state)

        if self.game_item_lib.is_absent(object_state):
            #self.item_label.text = ''
            #self.smell_label.text = ''
            self.visible = False
            self.inventory_item.visible = False
            self.item_indicator.visible = False
            self.smell.visible = False
        else:
            self.team = ObjectType.type_by_id(self.obj_id)
            self.image = self.resource_manager.get_frame(obj_type=self.team, state=self.state, carrying_food=self.carrying)
            self.upd_x, self.upd_y = self.game_item_lib.get_draw_coords(object_state)
            self.update(x=self.upd_x, y=self.upd_y)

            self.inventory_item.image = self.resource_manager.get_inventory_object(self.active_item)
            #off_x, off_y = self.rotate_bind_inventory[self.dir_x][self.dir_y]
            #self.inventory_item.update(x=x+off_x, y=y+off_y, rotation=self.rotate_bind[dir_x][dir_y])
            self.inventory_item.update(x=self.upd_x - np.int32(45), y=self.upd_y - np.int32(50))
            self.inventory_item.visible = True

            self.inv_item_count = Inventory.get_inventory_item_count(self.inventory, self.active_item)
            self.item_indicator.image = self.resource_manager.get_items_number(self.inv_item_count)
            #self.item_indicator.update(x=x+off_x, y=y+off_y, rotation=self.rotate_bind[dir_x][dir_y])
            self.item_indicator.update(x=self.upd_x+np.int32(15), y=self.upd_y-np.int32(50))
            self.item_indicator.visible = True

            self.smell.image = self.resource_manager.get_smell_indicator(self.current_smell)
            self.smell.update(x=self.upd_x, y=self.upd_y+np.int32(50))
            self.smell.visible = True

            #self.visible = True
            #self.item_label.text = self.item_bind[active_item]
            #self.item_label.x = self.x
            #self.item_label.y = self.y - 30#

            #self.smell_label.text = self.smell_bind[team][smell]
            #self.smell_label.x = self.x
            #self.smell_label.y = self.y + 30




class FoodRepr(pyglet.sprite.Sprite):
    def __init__(self, game_item_lib, resource_manager, batch):
        self.game_item_lib = game_item_lib
        self.resource_manager = resource_manager
        group = pyglet.graphics.OrderedGroup(1)
        image = self.resource_manager.get_frame(obj_type=ObjectType.Food, obj_subtype=ObjectSubtype.FoodApple, state=ObjectState.ObjectIdle)
        super(FoodRepr, self).__init__(img=image, batch=batch, group=group)
        self.obj_type, self.subtype, self.state = None, None, None
        self.visible = False

    def set_visible(self, visible=False):
        self.visible = visible

    def update_self(self, object_state, objects_state=None):
        self.obj_type, self.subtype, self.state = self.game_item_lib.get_props((ObjectProp.ObjType, ObjectProp.ObjSubtype, ObjectProp.State), object_state)

        if self.game_item_lib.is_absent(object_state):
            self.visible = False
        else:
            self.image = self.resource_manager.get_frame(obj_type=self.obj_type, obj_subtype=self.subtype, state=self.state)
            self.x, self.y = self.game_item_lib.get_draw_coords(object_state)
            self.visible = True



class ColonyRepr(pyglet.sprite.Sprite):
    def __init__(self, game_item_lib, game_items_lib, resource_manager, batch):
        self.game_item_lib = game_item_lib
        self.game_items_lib = game_items_lib
        self.resource_manager = resource_manager
        group = pyglet.graphics.OrderedGroup(4)
        image = self.resource_manager.get_frame(obj_type=ObjectType.Colony1, state=ObjectState.ObjectIdle)
        super(ColonyRepr, self).__init__(img=image, batch=batch, group=group)
        self.visible = False
        self.obj_id, self.state = None, None
        self.team = None

    def set_visible(self, visible=False):
        self.visible = visible

    def update_self(self, object_state, objects_state):
        self.obj_id, self.state = self.game_item_lib.get_props((ObjectProp.ObjId, ObjectProp.State), object_state)
        self.team = ObjectType.type_by_id(self.obj_id)
        self.image = self.resource_manager.get_frame(obj_type=self.team, state=self.state)
        self.x, self.y = self.game_item_lib.get_draw_coords(object_state)
        if self.game_item_lib.is_absent(object_state):
            self.visible = False
        else:
            self.visible = True
            self.image = self.resource_manager.get_frame(obj_type=self.team, state=self.state)
            self.x, self.y = self.game_item_lib.get_draw_coords(object_state)


class MainMenu(pyglet.sprite.Sprite):
    def __init__(self, game_item_lib, resource_manager, batch, screen_width, screen_height):
        self.game_item_lib = game_item_lib
        self.resource_manager = resource_manager
        image = pyglet.image.create(screen_width, screen_height)
        group = pyglet.graphics.OrderedGroup(6)
        super(MainMenu, self).__init__(img=image, batch=batch, group=group)
        self.visible = True

    def set_visible(self, visible=False):
        self.visible = visible

    def update_self(self):
        pass

class Background(pyglet.sprite.Sprite):
    def __init__(self, game_item_lib, resource_manager, batch, screen_width, screen_height):
        self.game_item_lib = game_item_lib
        self.resource_manager = resource_manager
        image = self.resource_manager.get_gui_element('background')
        group = pyglet.graphics.OrderedGroup(0)
        super(Background, self).__init__(img=image, batch=batch, group=group)
        self.visible = False

    def set_visible(self, visible=False):
        self.visible = visible



class ColonyPanel(pyglet.sprite.Sprite):
    def __init__(self, colony_type, game_item_lib, game_items_lib, resource_manager, batch, screen_width, screen_height):
        self.game_item_lib = game_item_lib
        self.game_items_lib = game_items_lib
        self.colony_type = colony_type
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.colony = None
        self.king = None
        self.threshold = np.int32(8000)
        self.y_thr = 250
        self.y_offset = 80
        self.label_x_offset = 200
        self.label_y_offset = 45
        self.font_size = 24
        self.x_draw = None
        self.y_draw = None

        self.resource_manager = resource_manager
        image = self.resource_manager.get_gui_element('colony_panel')
        self.panel_width = image.width
        self.panel_height = image.height
        group = pyglet.graphics.OrderedGroup(5)
        super(ColonyPanel, self).__init__(img=image, batch=batch, group=group)
        self.info_label = pyglet.text.Label('',
                                            font_name='Helvetica',
                                            font_size=self.font_size,
                                            color=(255, 255, 255, 255),
                                            x=0, y=0,
                                            anchor_x='center',  anchor_y='center',
                                            batch=batch,
                                            group=group)
        self.visible = False
        self.king_absent, self.colony_absent, self.carrying = None, None, None

    def set_visible(self, visible):
        self.visible = visible

    def update_coords(self):
        if self.x_draw is None:
            self.x_draw, self.y_draw = self.game_item_lib.get_draw_coords(self.colony)
            if self.x_draw < self.panel_width // 2:
                self.x_draw = self.panel_width//2 + 20
            elif self.x_draw > self.screen_width - self.panel_width // 2:
                self.x_draw = self.screen_width - self.panel_width // 2 + 20
            self.y_draw = self.y_draw - self.y_offset if self.y_draw > self.y_thr else self.y_draw + self.y_offset
            self.x, self.y = self.x_draw, self.y_draw
            self.info_label.x = self.x_draw - self.label_x_offset
            self.info_label.y = self.y_draw - self.label_y_offset

    def update_self(self, objects_copy):
        self.colony = self.game_items_lib.get_colony_by_object_type(self.colony_type, objects_copy)
        self.king = self.game_items_lib.get_king_by_object_type(self.colony_type, objects_copy)
        self.king_absent, self.colony_absent = self.game_item_lib.is_absent(self.king), self.game_item_lib.is_absent(self.colony)
        if not self.king_absent and not self.colony_absent and self.threshold > self.game_item_lib.dist_between_two_items(self.colony, self.king, euclid=False):
            self.update_coords()
            self.image = self.resource_manager.get_gui_element('colony_panel')
            self.visible = True
            self.carrying, = self.game_item_lib.get_props((ObjectProp.Carrying,), self.colony)
            self.info_label.text = str(self.carrying)
        else:
            self.info_label.text = ''
            self.visible = False

