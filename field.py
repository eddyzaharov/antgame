from __future__ import absolute_import, print_function
from threading import Thread
from multiprocessing import Process
import numpy as np
import pygame
import messages
from objects_template import *

class FieldState:
    Start, Pause, Run, Exit = range(4)

gpu_prog_text = """
        __kernel void diffusion(
            __global int *current_buffer, __global int *current_item_buffer, __global int *tmp_buffer,  __global int *diff_factor_buffer,  __global int *diffusion_coef_buffer,  int width, int height, int layers)
        {
          int xid = get_global_id(0);
          int yid = get_global_id(1);
          int zid = get_global_id(2);

          int left;
          int right;
          int down;
          int up;
          int current;
          int current_item;
          int current_diff_factor = diff_factor_buffer[zid];
          int current_diffusion_coef = diffusion_coef_buffer[zid];
          //current = current_buffer[xid*height + yid];
          current = current_buffer[xid*height*layers + yid*layers + zid];
          current_item = current_item_buffer[xid*height*layers + yid*layers+ zid];

          if(xid < 1)
            left = 0;
          else
            left = current_buffer[layers*height*(xid - 1) + layers*yid + zid];
          if(xid > width - 2)
            right = 0;
          else
            right = current_buffer[layers*height*(xid+1) + layers*yid + zid];
          if(yid > 0)
            down = current_buffer[layers*height*xid + layers*(yid-1) + zid];
          else
            down = 0;
          if(yid > height - 2)
            up = 0;
          else
            up = current_buffer[layers*height*xid + layers*(yid+1) + zid];

          int to_save;
          to_save = current + current_diffusion_coef *(left + right - 2*current + down + up - 2*current) / 100 - current_diff_factor + current_item;
          if (to_save < 0)
            to_save = 0;
          if (to_save > 1000)
            to_save = 1000;
          tmp_buffer[layers*height*xid + layers*yid + zid] = to_save;
          //tmp_buffer[xid + width*yid] = to_save;
        }
        """


class Field(Process):
    def __init__(self, messenger,  verbose_fps=True):
        super(Field, self).__init__()
        self.field_state = FieldState.Start
        self.messenger = messenger
        self.verbose_fps = verbose_fps
        self.frame_rate = np.int32(5)
        self.field_width = None
        self.field_height = None
        self.layers = FieldSmell.Total
        self.diffusion_coef = None
        self.diff_factor = None
        self.current_field = None
        self.future_field = None
        self.current_items_field = None
        self.future_item_field = None
        self.cycles = np.int32(0)
        self.clock = pygame.time.Clock()
        self.current = np.int32(0)
        self.left_x = np.int32(0)
        self.right_x = np.int32(0)
        self.down_y = np.int32(0)
        self.up_y = np.int32(0)
        self.new_val = np.int32(0)
        self.functions = {messages.Field.TerminateSimulation: self.terminate_simulation,
                          messages.Field.SetNewItemField: self.set_new_item_field,
                          messages.Field.PauseSimulation: self.pause_simulation,
                          messages.Field.RunSimulation: self.run_simulation,
                          messages.Field.SetFieldSettings: self.set_field_settings}
        self.field_state = FieldState.Pause

    def run_simulation(self):
        self.field_state = FieldState.Run

    def pause_simulation(self):
        self.field_state = FieldState.Pause

    def terminate_simulation(self):
        print("terminate field")
        self.field_state = FieldState.Exit

    def set_field_settings(self, field_width, field_height, configuration):
        self.field_width = field_width
        self.field_height = field_height
        self.diffusion_coef = np.zeros(self.layers, dtype=np.int32)
        self.diff_factor = np.zeros(self.layers, dtype=np.int32)
        for key in configuration:
            dcoef, dfact = configuration[key]
            if key == 'Ant':
                self.diffusion_coef[FieldSmell.Ant1] = dcoef
                self.diff_factor[FieldSmell.Ant1] = dfact
                self.diffusion_coef[FieldSmell.Ant2] = dcoef
                self.diff_factor[FieldSmell.Ant2] = dfact
            elif key == 'Colony':
                self.diffusion_coef[FieldSmell.Colony1] = dcoef
                self.diff_factor[FieldSmell.Colony1] = dfact
                self.diffusion_coef[FieldSmell.Colony2] = dcoef
                self.diff_factor[FieldSmell.Colony2] = dfact
            if key == 'Food':
                self.diffusion_coef[FieldSmell.Food] = dcoef
                self.diff_factor[FieldSmell.Food] = dfact
        print("Setting field configuration")
        self.current_field = np.zeros((self.field_width, self.field_height, self.layers), dtype=np.int32)
        self.future_field = np.zeros((self.field_width, self.field_height, self.layers), dtype=np.int32)
        self.current_items_field = np.zeros((self.field_width, self.field_height, self.layers), dtype=np.int32)
        self.future_item_field = np.zeros((self.field_width, self.field_height, self.layers), dtype=np.int32)

    def run(self):
        while self.field_state != FieldState.Exit:
            while True:
                data = self.messenger.get_message(messages.Field)
                if not data:
                    break
                self.functions[data['func']](**data['args']) if 'args' in data else self.functions[data['func']]()
            if self.field_state != FieldState.Pause and self.current_field is not None:
                self.loop_tick()
                #self.messenger.renderer_update_field(np.copy(self.current_field))
                self.messenger.objects_update_field(np.copy(self.current_field))
                #self.messenger.controls_update_field(np.copy(self.current_field))
                self.messenger.game_update_field(np.copy(self.current_field))
            elapsed = self.clock.tick(self.frame_rate)
            if self.cycles % 10 == 0 and self.verbose_fps:
                print("field elapsed: {}".format(elapsed))
            self.cycles += 1
        print("exiting out of field")

    def get_neighbor_value(self, x, y, x_inc, y_inc, layer):
        #new_x, new_y = x + x_inc, y + y_inc
        return 0 if x + x_inc < 0 or x + x_inc >= self.field_width or y + y_inc < 0 or y + y_inc >= self.field_height else self.current_field[x + x_inc, y + y_inc, layer]

    def calc_new_value(self, x, y, layer):
        self.current = self.current_field[x, y, layer]
        self.left_x = self.current_field[x - 1, y, layer] if x > np.int32(0) else np.int32(0)
        self.right_x = self.current_field[x + 1, y, layer] if x < self.field_width - 2 else np.int32(0)
        self.down_y = self.current_field[x, y - 1, layer] if y > 0 else np.int32(0)
        self.up_y = self.current_field[x, y + 1, layer] if y < self.field_height - 2 else np.int32(0)
        self.new_val = np.int32(round(self.current + 0.01 * self.diffusion_coef[layer] * (self.left_x + self.right_x - 2 * self.current + self.down_y + self.up_y - 2 * self.current_field[x, y, layer])
                                      - self.diff_factor[layer] + self.current_items_field[x, y, layer]))
        if self.new_val < 0:
            self.new_val = np.int32(0)
        elif self.new_val > 1000:
            self.new_val = np.int32(1000)
        return self.new_val

    def calc_new_value_np(self):
        field_iter = np.nditer(self.future_field, flags=['multi_index'],  op_flags=['readwrite'])
        while not field_iter.finished:

            field_iter[0] = self.calc_new_value(field_iter.multi_index[0], field_iter.multi_index[1], field_iter.multi_index[2])
            field_iter.iternext()

        self.future_field, self.current_field = self.current_field, self.future_field

    def set_new_item_field(self, field):
        del self.current_items_field
        self.current_items_field = field

    def loop_tick(self):
        self.calc_new_value_np()

    def get_current_field(self):
        return self.current_field
