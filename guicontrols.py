from threading import Thread
from multiprocessing import Process
import pygame
import messages
from pyglet.window import key as pygletkey
from objects_template import *
from lib_obj import GameItem, GameItems, Inventory

class GUIcontrolsState:
    Start, InMenu, InGame, Exit = range(4)

class MouseButton:
    Left, Middle, Right = range(3)
    ScrollUp, ScrollDown = 4, 5


class GUIcontrols(Process):
    def __init__(self, messenger, screen_width, screen_height, resource_manager, frame_rate=30, verbose_fps=False):
        super(GUIcontrols, self).__init__()
        self.gui_state = GUIcontrolsState.Start
        self.messenger = messenger
        self.resource_manager = resource_manager
        self.frame_rate = frame_rate
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.objects_width = None
        self.objects_height = None
        self.field_width = None
        self.field_height = None
        self.field_copy = None
        self.objects_copy = None
        self.player_direction_x = 0
        self.player_direction_y = 0
        self.verbose_fps = verbose_fps
        self.cycles = 0
        self.clock = pygame.time.Clock()
        self.game_item_lib = GameItem(screen_width, screen_height)
        self.game_items_lib = GameItems()
        self.mouse_control = MouseControl(ObjectType.King1, self.game_item_lib, self.game_items_lib, self.resource_manager, screen_width, screen_height, self.messenger)
        self.kb_control = KbControl(ObjectType.King2, self.game_item_lib, self.game_items_lib, self.resource_manager, screen_width, screen_height, self.messenger)
        self.gui_state = GUIcontrolsState.InMenu
        self.functions = {messages.GuiControls.StopGui: self.stop_gui,
                          messages.GuiControls.StartGame: self.start_game,
                          messages.GuiControls.UpdateObjects: self.update_objects,
                          messages.GuiControls.UpdateField: self.update_field,
                          messages.GuiControls.HandleKey: self.handle_kb_event,
                          messages.GuiControls.HandleMouse: self.handle_mb_push}

    def run(self):
        while self.gui_state != GUIcontrolsState.Exit:
            while True:
                data = self.messenger.get_message(messages.GuiControls)
                if not data:
                    break
                self.functions[data['func']](**data['args']) if 'args' in data else self.functions[data['func']]()
            #self.handle_events()
            elapsed = self.clock.tick(self.frame_rate)
            if self.cycles % 30 == 0 and self.verbose_fps:
                print("controls elapsed: {}".format(elapsed))
            self.cycles += 1

    def update_field(self, field):
        self.field_copy = field

    def update_objects(self, objects):
        del self.objects_copy
        self.objects_copy = objects
        self.mouse_control.update_objects_copy(objects)
        self.kb_control.update_objects_copy(objects)

    def start_game(self, objects_width, objects_height, field_width, field_height):
        self.objects_width = objects_width
        self.objects_height = objects_height
        self.field_width = field_width
        self.field_height = field_height
        self.mouse_control.set_objects_size(objects_width, objects_height)
        self.kb_control.set_objects_size(objects_width, objects_height)
        self.game_item_lib.set_game_settings(objects_width, objects_height, field_width, field_height)
        self.gui_state = GUIcontrolsState.InGame

    def stop_gui(self):
        print("terminating controls")
        self.gui_state = GUIcontrolsState.Exit

    def handle_kb_event(self, pushed, key):
        if self.gui_state == GUIcontrolsState.InMenu:
            if pushed:
                if key == pygletkey.SPACE:
                    self.gui_state = GUIcontrolsState.InGame
                    self.messenger.game_run_new_game()
                    return
                elif key == pygletkey.Q:
                    self.messenger.game_quit()
                    return
        elif self.gui_state == GUIcontrolsState.InGame:
            self.kb_control.dispatch_kb_event(pushed, key)

    def handle_mb_push(self, button, x, y):
        if self.gui_state == GUIcontrolsState.InGame:
            self.mouse_control.dispatch_mouse_event(button, x, y)


class BaseControl:
    def __init__(self, player, game_item_lib, game_items_lib, resource_manager, screen_width, screen_height, messenger):
        self.game_item_lib = game_item_lib
        self.game_items_lib = game_items_lib
        self.resource_manager = resource_manager
        self.messenger = messenger
        image = self.resource_manager.get_gui_element('colony_panel')
        self.threshold = np.int32(8000)
        self.y_wide = image.height
        self.x_wide = image.width
        self.y_thr = 250
        self.y_offset = 80
        self.colony = None
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.objects_width = None
        self.objects_height = None
        self.x_ratio = None
        self.y_ratio = None
        self.player = player

    def set_objects_size(self, objects_width, objects_height):
        self.objects_width = objects_width
        self.objects_height = objects_height
        self.x_ratio = objects_width / self.screen_width
        self.y_ratio = objects_height / self.screen_height

class MouseControl(BaseControl):
    def __init__(self, player, game_item_lib, game_items_lib, resource_manager, screen_width, screen_height, messenger):
        super(MouseControl, self).__init__(player, game_item_lib, game_items_lib, resource_manager, screen_width, screen_height, messenger)
        self.objects_copy = None
        self.edge_x = None
        self.edge_y = None
        self.buy_bind = {2: Inventory.TeleportInd,
                         3: Inventory.BombInd,
                         4: Inventory.HealerInd,
                         5: Inventory.FreezeInd}

    def update_objects_copy(self, objects_copy):
        del self.objects_copy
        self.objects_copy = objects_copy

    def dispatch_mouse_event(self, button, x, y):
        if self.objects_width is None or self.objects_height is None:
            return
        x_objects = x // (self.screen_width / self.objects_width)
        y_objects = y // (self.screen_height / self.objects_height)
        if button == MouseButton.Right:
            self.messenger.objects_king_set_destination(self.player, x_objects, y_objects)
        elif button == MouseButton.ScrollUp:
            self.messenger.objects_king_change_item(self.player, False)
        elif button == MouseButton.ScrollDown:
            self.messenger.objects_king_change_item(self.player, True)
        elif button == MouseButton.Middle:
            self.messenger.objects_king_switch_smell(self.player, True)
        elif button == MouseButton.Left:
            panel_index = self.get_panel_index(x, y)
            if panel_index is None:
                self.messenger.objects_king_use_item(self.player)
            if panel_index == 0:
                self.messenger.objects_king_train_unit(self.player, ObjectSubtype.AntWorker)
            elif panel_index == 1:
                self.messenger.objects_king_train_unit(self.player, ObjectSubtype.AntWarrior)
            elif panel_index in self.buy_bind:
                self.messenger.objects_king_buy_item(self.player, self.buy_bind[panel_index])

    def get_panel_index(self, x, y):
        if self.objects_copy is None or self.objects_width is None or self.objects_height is None:
            return None
        self.colony = self.game_items_lib.get_colony_by_object_type(self.player, self.objects_copy)
        self.king = self.game_items_lib.get_king_by_object_type(self.player, self.objects_copy)
        if self.edge_x is None:
            x_draw, y_draw = self.game_item_lib.get_draw_coords(self.colony)
            if x_draw < self.x_wide // 2:
                x_draw = self.x_wide//2 + 20
            elif x_draw > self.screen_width - self.x_wide // 2:
                x_draw = self.screen_width - self.x_wide // 2 + 20
            y_draw = y_draw - self.y_offset if y_draw > self.y_thr else y_draw + self.y_offset
            self.edge_x, self.edge_y = x_draw - self.x_wide//2, y_draw - self.y_wide//2
        index = None

        king_absent, colony_absent = self.game_item_lib.is_absent(self.king), self.game_item_lib.is_absent(self.colony)
        if not king_absent and not colony_absent and self.threshold > self.game_item_lib.dist_between_two_items(self.colony, self.king, euclid=False):
            if self.edge_y <= y <= self.edge_y + self.y_wide:
                if self.edge_x <= x < self.edge_x + self.x_wide:
                    index = int((x - self.edge_x) // (self.x_wide / Constants.CountBuyableItems))
        return index


class KbControl(BaseControl):
    def __init__(self, player, game_item_lib, game_items_lib, resource_manager, screen_width, screen_height, messenger):
        super(KbControl, self).__init__(player, game_item_lib, game_items_lib, resource_manager, screen_width, screen_height, messenger)
        self.player_direction_x = 0
        self.player_direction_y = 0
        self.objects_copy = None
        self.dir_x, self.dir_y = 0, 0
        self.direction_bind_y = None
        self.direction_bind_x = None
        self.buy_bind = {pygletkey._3: Inventory.TeleportInd,
                         pygletkey._4: Inventory.BombInd,
                         pygletkey._5: Inventory.HealerInd,
                         pygletkey._6: Inventory.FreezeInd}

    def update_objects_copy(self, objects_copy):
        del self.objects_copy
        self.objects_copy = objects_copy

    def dispatch_kb_event(self, pushed, key):
        if pushed:
            if key == pygletkey.LEFT:
                self.messenger.objects_king_change_item(self.player, False)
            elif key == pygletkey.RIGHT:
                self.messenger.objects_king_change_item(self.player, True)
            elif key == pygletkey.UP:
                self.messenger.objects_king_switch_smell(self.player, True)
            elif key == pygletkey.DOWN:
                self.messenger.objects_king_switch_smell(self.player, False)
            elif key == pygletkey.SPACE:
                self.messenger.objects_king_use_item(self.player)

            elif key == pygletkey._1:
                self.messenger.objects_king_train_unit(self.player, ObjectSubtype.AntWorker)
            elif key == pygletkey._2:
                self.messenger.objects_king_train_unit(self.player, ObjectSubtype.AntWarrior)
            elif key in self.buy_bind:
                self.messenger.objects_king_buy_item(self.player, self.buy_bind[key])

        if key in (pygletkey.W, pygletkey.S, pygletkey.D, pygletkey.A):
                self.recalc_player_direction(pushed, key)
                self.change_player_direction()

    def recalc_player_direction(self, pushed, key):
        self.dir_x, self.dir_y = self.player_direction_x, self.player_direction_y
        if pushed:
            self.direction_bind_y = {pygletkey.S: self.dir_y - 1 if self.dir_y > -1 else -1,
                                     pygletkey.W: self.dir_y + 1 if self.dir_y < 1 else 1}
            self.direction_bind_x = {pygletkey.A: self.dir_x - 1 if self.dir_x > -1 else -1,
                                     pygletkey.D: self.dir_x + 1 if self.dir_x < 1 else 1}
            if key in self.direction_bind_y:
                self.player_direction_y = self.direction_bind_y[key]
            if key in self.direction_bind_x:
                self.player_direction_x = self.direction_bind_x[key]
        else:
            self.direction_bind_y = {pygletkey.S: self.dir_y + 1 if self.dir_y < 1 else 1,
                                     pygletkey.W: self.dir_y - 1 if self.dir_y > -1 else -1}
            self.direction_bind_x = {pygletkey.A: self.dir_x + 1 if self.dir_x < 1 else 1,
                                     pygletkey.D: self.dir_x - 1 if self.dir_x > -1 else -1}
            if key in self.direction_bind_y:
                self.player_direction_y = self.direction_bind_y[key]
            if key in self.direction_bind_x:
                self.player_direction_x = self.direction_bind_x[key]

    def change_player_direction(self):
        self.messenger.objects_king_set_direction(self.player, self.player_direction_x, self.player_direction_y)
