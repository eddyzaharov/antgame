import numpy as np
from objects_template import *

class GameItem:
    def __init__(self, screen_width, screen_height, objects_width=None, objects_height=None, field_width=None, field_height=None):
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.field_width = field_width
        self.field_height = field_height
        self.field_layers = FieldSmell.Total
        self.objects_width = objects_width
        self.objects_height = objects_height

    def set_game_settings(self, objects_width, objects_height, field_width, field_height):
        self.objects_width = objects_width
        self.objects_height = objects_height
        self.field_width = field_width
        self.field_height = field_height

    @staticmethod
    def dist_between_two_items(item_1, item_2, euclid=True):
        if euclid:
            return np.sqrt((item_1[ObjectProp.Xcoord] - item_2[ObjectProp.Xcoord])**2 + (item_1[ObjectProp.Ycoord] - item_2[ObjectProp.Ycoord]) ** 2)
        else:
            return np.abs(item_1[ObjectProp.Xcoord] - item_2[ObjectProp.Xcoord]) + np.abs(item_1[ObjectProp.Ycoord] - item_2[ObjectProp.Ycoord])

    @staticmethod
    def destination_distance(item, euclid=True):
        if euclid:
            return np.sqrt((item[ObjectProp.Xcoord] - item[ObjectProp.DestinationX]) ** 2 + (item[ObjectProp.Ycoord] - item[ObjectProp.DestinationY]) ** 2)
        else:
            return np.abs(item[ObjectProp.Xcoord] - item[ObjectProp.DestinationX]) + np.abs(item[ObjectProp.Ycoord] - item[ObjectProp.DestinationY])

    def get_draw_coords(self, object_state):
        return np.int32(object_state[ObjectProp.Xcoord] * self.screen_width / self.objects_width), np.int32(object_state[ObjectProp.Ycoord] * self.screen_height / self.objects_height)

    @staticmethod
    def get_props(props, object_state):
        return (object_state[prop] for prop in props)

    @staticmethod
    def set_props(props, object_state):
        for key in props:
            object_state[key] = props[key]

    @staticmethod
    def increment_props(props, object_state):
        for key in props:
            object_state[key] += props[key]

    @staticmethod
    def is_absent(object_state):
        return object_state[ObjectProp.ObjType] == ObjectType.Absent

    @staticmethod
    def wipe_object(object_state):
        for i in range(ObjectProp.ObjType, ObjectProp.Total):
            object_state[i] = np.int32(0)

    @staticmethod
    def generate_new_object_with_id(ind, obj_type, obj_subtype, x, y, energy):
        return np.array([ind, obj_type, obj_subtype, x, y, 0, 0, 0, ObjectState.ObjectIdle, 0, energy, 0, 0, 0, 0, 0, 0, 0, 0, 0])

class GameItems:
    def __init__(self):
        pass

    @staticmethod
    def get_object_by_id(obj_id, objects_state):
        return objects_state[obj_id]

    @staticmethod
    def get_colonies(objects_state):
        return objects_state[200], objects_state[201]

    @staticmethod
    def get_kings(objects_state):
        return objects_state[0], objects_state[100]

    @staticmethod
    def get_king_by_object_type(object_type, objects_state):
        if object_type in (ObjectType.Ant1, ObjectType.Colony1, ObjectType.King1):
            return objects_state[0]
        elif object_type in (ObjectType.Ant2, ObjectType.Colony2, ObjectType.King2):
            return objects_state[100]

    @staticmethod
    def get_colony_by_object_type(object_type, objects_state):
        if object_type in (ObjectType.Ant1, ObjectType.Colony1, ObjectType.King1):
            return objects_state[200]
        elif object_type in (ObjectType.Ant2, ObjectType.Colony2, ObjectType.King2):
            return objects_state[201]

    @staticmethod
    def generate_empty_objects():
        current_objects = np.zeros((Constants.ObjArrayTotal, ObjectProp.Total), dtype=np.int32)
        current_objects[:, 0] = np.arange(Constants.ObjArrayTotal)
        return current_objects


class Inventory:
    NoObject, TeleportInd, BombInd, HealerInd, FreezeInd = 0, 1, 2, 3, 4
    Cost = {TeleportInd : np.int32(10),
            BombInd: np.int32(10),
            HealerInd: np.int32(10),
            FreezeInd: np.int32(10)}

    def __init__(self):
        self.binding_forward = [1, 2, 3, 4, 0] #what index use next 0 ->1, 1->2...
        self.binding_backwards = [4, 0, 1, 2, 3] # 0 -> 4, 1 ->0

    @staticmethod
    def get_inventory_formatted(player_state):
        out_inventory = {}
        inventory, active_item = GameItem.get_props((ObjectProp.Inventory, ObjectProp.ActiveItem), player_state)
        for item_ind in range(1, 5):
            out_inventory[item_ind] = inventory // 10**(item_ind-1) % 10
        return out_inventory, active_item

    @staticmethod
    def get_inventory_item_count(inventory, item_ind):
        if item_ind == 0:
            return 0
        return inventory // 10**(item_ind-1) % 10

    def add_remove_item(self, item_type, player_state, add=True):
        inventory, active_item = GameItem.get_props((ObjectProp.Inventory, ObjectProp.ActiveItem), player_state)
        items_count = np.int32(inventory // 10**(item_type-1) % 10)
        if add:
            if items_count > 3:
                return False
            else:
                GameItem.set_props({ObjectProp.Inventory: np.int32(inventory + 10**(item_type-1))}, player_state)
                return True
        else:
            if items_count < 1:
                return False
            else:
                GameItem.set_props({ObjectProp.Inventory: np.int32(inventory - 10**(item_type-1))}, player_state)
                if items_count == 1:
                    next_active = item_type
                    while True:
                        next_active = self.binding_forward[next_active]
                        if inventory // 10**(next_active-1) % 10 > 0 or next_active == 0:
                            GameItem.set_props({ObjectProp.ActiveItem: next_active}, player_state)
                            break
                return True

    def change_active_item(self, player_state, forward=True):
        inventory, active_item = GameItem.get_props((ObjectProp.Inventory, ObjectProp.ActiveItem), player_state)
        if forward:
            bind = self.binding_forward
        else:
            bind = self.binding_backwards

        next_index = bind[active_item]
        while next_index != 0:
           items_count = inventory // 10 ** (next_index-1) % 10
           if items_count < 1:
                next_index = bind[next_index]
                continue
           else:
                GameItem.set_props({ObjectProp.ActiveItem: next_index}, player_state)
                return
        GameItem.set_props({ObjectProp.ActiveItem: next_index}, player_state)
        return

