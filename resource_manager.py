from objects_template import *
import pyglet
from lib_obj import Inventory


pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
pyglet.gl.glBlendFunc(pyglet.gl.GL_SRC_ALPHA, pyglet.gl.GL_ONE_MINUS_SRC_ALPHA)
pyglet.options['debug_gl'] = False

class ResourceManager:
    def __init__(self):
        self.ant = None
        self.king = None
        self.colony = None
        self.food = None
        self.gui = None
        self.inventory = None
        self.items_number = None
        self.smell_indicators = None
        self.frame = None
        #self.ant1_worker = self.load_pic('ant1_worker.png')
        #self.ant1_worker_with_food = self.load_pic('ant1_worker_with_food.png')
        #self.ant1_warrior = self.load_pic('ant1_warrior.png')
        #self.ant1_king = self.load_pic('ant1_king.png')
        #self.ant1_colony = self.load_pic('ant1_colony.png')
        #self.ant2_worker = self.load_pic('ant2_worker.png')
        #self.ant2_worker_with_food = self.load_pic('ant2_worker_with_food.png')
        #self.ant2_warrior = self.load_pic('ant2_warrior.png')
        #self.ant2_king = self.load_pic('ant2_king.png')
        #self.ant2_colony = self.load_pic('ant2_colony.png')
        #self.pic_food = self.load_pic('food.png')
        #self.pic_little_food = self.load_pic('little_food.png')
        self.deploy_all()

    def load_pic(self, path, anchor_edge=False):
        img = pyglet.image.load('resources/'+path)
        if anchor_edge:
            img.anchor_x = 0
            img.anchor_y = 0
        else:
            img.anchor_x = img.width // 2
            img.anchor_y = img.height // 2
        return img

    def deploy_all(self):
        self.deploy_ant()
        self.deploy_king()
        self.deploy_colony()
        self.deploy_food()
        self.deploy_gui()
        self.deploy_inventory()
        self.deploy_smell_indicators()

    def deploy_ant(self):

        self.ant = {}
        for team in (ObjectType.Ant1, ObjectType.Ant2):
            for subtype in (ObjectSubtype.AntWorker, ObjectSubtype.AntWarrior):
                for state in (ObjectState.ObjectIdle, ObjectState.AntSearchingFood, ObjectState.AntSearchingEnemy, ObjectState.AntAttacking, ObjectState.AntGathering,
                              ObjectState.AntGoingHome, ObjectState.AntRunaway, ObjectState.Healing, ObjectState.AntTeleported, ObjectState.AntBombed,
                              ObjectState.AntFreezed, ObjectState.ObjectDestroyed):
                    for carrying_food in (True, False):
                        if team not in self.ant:
                            self.ant[team] = {}
                        if subtype not in self.ant[team]:
                            self.ant[team][subtype] = {}
                        if state not in self.ant[team][subtype]:
                            self.ant[team][subtype][state] = {}
                        if carrying_food not in self.ant[team][subtype][state]:
                            self.ant[team][subtype][state][carrying_food] = {}
                        if carrying_food:
                            pass
                        if team == ObjectType.Ant1:
                            if subtype == ObjectSubtype.AntWarrior:
                                self.ant[team][subtype][state][carrying_food] = self.load_pic('ant1_warrior.png')
                            elif subtype == ObjectSubtype.AntWorker:
                                if carrying_food:
                                    self.ant[team][subtype][state][carrying_food] = self.load_pic('ant1_worker_with_food.png')
                                else:
                                    self.ant[team][subtype][state][carrying_food] = self.load_pic('ant1_worker.png')

                        elif team == ObjectType.Ant2:
                            if subtype == ObjectSubtype.AntWarrior:
                                self.ant[team][subtype][state][carrying_food] = self.load_pic('ant2_warrior.png')
                            elif subtype == ObjectSubtype.AntWorker:
                                if carrying_food:
                                    self.ant[team][subtype][state][carrying_food] = self.load_pic('ant2_worker_with_food.png')
                                else:
                                    self.ant[team][subtype][state][carrying_food] = self.load_pic('ant2_worker.png')

    def deploy_king(self):
        self.king = {}
        for team in (ObjectType.King1, ObjectType.King2):
            for state in (ObjectState.ObjectIdle, ObjectState.ObjectDestroyed, ObjectState.KingFree, ObjectState.Healing, ObjectState.KingAutoAttack,
                          ObjectState.KingGatherFood, ObjectState.KingTeleporting, ObjectState.KingHealing, ObjectState.KingBombing, ObjectState.KingFreezing):
                for carrying_food in (True, False):
                    if team not in self.king:
                        self.king[team] = {}
                    if state not in self.king[team]:
                        self.king[team][state] = {}
                    if carrying_food not in self.king[team][state]:
                        self.king[team][state][carrying_food] = {}
                    if team == ObjectType.King1:
                        if carrying_food:
                            self.king[team][state][carrying_food] = self.load_pic('ant1_king_with_food.png')
                        else:
                            self.king[team][state][carrying_food] = self.load_pic('ant1_king.png')
                    elif team == ObjectType.King2:
                        if carrying_food:
                            self.king[team][state][carrying_food] = self.load_pic('ant2_king_with_food.png')
                        else:
                            self.king[team][state][carrying_food] = self.load_pic('ant2_king.png')

    def deploy_food(self):
        self.food = {}
        for food_subtype in (ObjectSubtype.FoodApple, ObjectSubtype.FoodCorpse):
            for state in (ObjectState.ObjectIdle, ObjectState.ObjectDestroyed):
                if food_subtype not in self.food:
                    self.food[food_subtype] = {}
                if state not in self.food[food_subtype]:
                    self.food[food_subtype][state] = {}
                self.food[food_subtype][state] = self.load_pic('food.png')

    def deploy_colony(self):
        self.colony = {}
        for team in (ObjectType.Colony1, ObjectType.Colony2):
            for state in (ObjectState.ObjectIdle, ObjectState.ObjectDestroyed):
                if team not in self.colony:
                    self.colony[team] = {}
                if state not in self.colony[team]:
                    self.colony[team][state] = {}
                if team == ObjectType.Colony1:
                    self.colony[team][state] = self.load_pic('ant1_colony.png')
                elif team == ObjectType.Colony2:
                    self.colony[team][state] = self.load_pic('ant2_colony.png')

    def deploy_gui(self):
        self.gui = {'colony_panel': self.load_pic('colony_panel.png'),
                    'background': self.load_pic('background.png', anchor_edge=True)}

    def deploy_inventory(self):
        self.inventory = {Inventory.NoObject: self.load_pic('back_noobject.png'),
                          Inventory.TeleportInd: self.load_pic('back_teleport.png'),
                          Inventory.BombInd: self.load_pic('back_bomb.png'),
                          Inventory.HealerInd: self.load_pic('back_healer.png'),
                          Inventory.FreezeInd: self.load_pic('back_freezer.png')}
        self.items_number = {0: self.load_pic('item_0.png'),
                             1: self.load_pic('item_1.png'),
                             2: self.load_pic('item_2.png'),
                             3: self.load_pic('item_3.png'),
                             4: self.load_pic('item_4.png')}

    def deploy_smell_indicators(self):
        self.smell_indicators = {InfoSmell.NoSmell: self.load_pic('smell_nothing.png'),
                                 InfoSmell.Food: self.load_pic('smell_food.png'),
                                 InfoSmell.Ant1: self.load_pic('smell_enemy.png'),
                                 InfoSmell.Ant2: self.load_pic('smell_enemy.png'),
                                 InfoSmell.Colony1: self.load_pic('smell_home.png'),
                                 InfoSmell.Colony2: self.load_pic('smell_home.png')}

    def get_smell_indicator(self, smell):
        return self.smell_indicators[smell]

    def get_inventory_object(self, object_type):
        return self.inventory[object_type]

    def get_items_number(self, number):
        if number > 3:
            return self.items_number[4]
        else:
            return self.items_number[number]

    def get_gui_element(self, index_name):
        return self.gui[index_name]

    def get_frame(self, obj_type=None, obj_subtype=None, state=None, carrying_food=None):
        self.frame = None
        if obj_type == ObjectType.Ant1 or obj_type == ObjectType.Ant2:
            carrying_food = True if carrying_food > 0 else False
            self.frame = self.ant[obj_type][obj_subtype][state][carrying_food]
        elif obj_type == ObjectType.King1 or obj_type == ObjectType.King2:
            carrying_food = True if carrying_food > 0 else False
            self.frame = self.king[obj_type][state][carrying_food]
        elif obj_type == ObjectType.Colony1 or obj_type == ObjectType.Colony2:
            self.frame = self.colony[obj_type][state]
        elif obj_type == ObjectType.Food:
            self.frame = self.food[obj_subtype][state]
        return self.frame
