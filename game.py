from __future__ import absolute_import, print_function
import pygame
import pyglet
from pyglet.window import mouse as mousebutton
from field import Field
from objects import Objects
from guiscreen import GUIscreen
from guicontrols import GUIcontrols
from renderer import Renderer
import random
from objects_template import *
from resource_manager import ResourceManager
import time
import messages
from guicontrols import MouseButton
from messages import Messenger
import sys

class GameState:
    Start, ActiveGame, Menu, Exit = range(4)

class PygletGame:
    def __init__(self, screen_width, screen_height, verbose_fps=True):
        self.game_state = GameState.Start
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.verbose_fps = verbose_fps
        self.counter = None
        self.main_batch = None
        self.game_window = None
        self.field_width = 32
        self.field_height = 18
        self.objects_width = 160000
        self.objects_height = 90000
        self.fps_display = pyglet.clock.ClockDisplay()
        self.messenger = Messenger()
        self.resource_manager = ResourceManager()
        self.renderer = Renderer(self.screen_width, self.screen_height, resource_manager=self.resource_manager)

        self.field = Field(self.messenger, verbose_fps=self.verbose_fps)
        self.field.start()

        self.gui_controls = GUIcontrols(self.messenger, screen_width, screen_height, resource_manager=self.resource_manager, verbose_fps=self.verbose_fps)
        self.gui_controls.start()

        self.objects = Objects(messenger=self.messenger, screen_width=screen_width, screen_height=screen_height, verbose_fps=self.verbose_fps)
        self.objects.start()
        self.game_state = GameState.Menu
        self.data = None
        self.functions = {messages.Game.Quit: self.quit,
                          messages.Game.UpdateField: self.update_field,
                          messages.Game.UpdateObjects: self.update_objects,
                          messages.Game.RunNewGame: self.run_new_game}

    def quit(self):
        self.game_state = GameState.Exit
        self.messenger.shutdown()
        self.objects.join()
        self.field.join()
        self.gui_controls.join()
        pyglet.app.exit()

    def update_field(self, field_copy):
        self.renderer.update_field(field_copy)

    def update_objects(self, objects_copy):
        self.renderer.update_objects(objects_copy)

    def update_graphics(self, dt):
        self.renderer.update_graphics()
        self.game_window.clear()
        self.renderer.batch.draw()

    def run_new_game(self):
        configuration = {ObjectType.Ant1: [],
                         ObjectType.King1: [],
                         ObjectType.Colony1: [],
                         ObjectType.Ant2: [],
                         ObjectType.King2: [],
                         ObjectType.Colony2: [],
                         ObjectType.Food: []}
        colony_offset_x, colony_offset_y = 36000, 22000
        colony1 = (self.objects_width - colony_offset_x, colony_offset_y)
        colony2 = (colony_offset_x, self.objects_height - colony_offset_y)
        for i in range(0, 3):
            configuration[ObjectType.Ant1].append((ObjectSubtype.AntWorker, colony1[0] + random.randint(-5000, 5000), colony1[1] + random.randint(-5000, 5000)))
        for i in range(0, 3):
            configuration[ObjectType.Ant2].append((ObjectSubtype.AntWorker, colony2[0] + random.randint(-5000, 5000), colony2[1] + random.randint(-5000, 5000)))

        #for i in range(0, 25):
        #    configuration[ObjectType.Ant1].append((ObjectSubtype.AntWarrior, colony1[0] + random.randint(-5000, 5000), colony1[1] + random.randint(-5000, 5000)))
        #    configuration[ObjectType.Ant2].append((ObjectSubtype.AntWarrior, colony2[0] + random.randint(-5000, 5000), colony2[1] + random.randint(-5000, 5000)))
        food_num = 15
        cosx, sinx = self.objects_width / np.sqrt(self.objects_width**2+self.objects_height**2), self.objects_height/ np.sqrt(self.objects_width**2+self.objects_height**2)
        print("sin {} {} ".format(cosx, sinx))
        for i in range(0, 10):
            while True:
                new_x = np.abs(np.random.randint(10000, (self.objects_width-10000)//cosx))
                new_y = int(np.random.normal(0, 10000))
                set_x, set_y = int(new_x*cosx - new_y*sinx), int(new_x*sinx + new_y*cosx)
                #set_x, set_y = new_x, new_y + self.objects_height//2
                print("setx {} {}".format(set_x,set_y))
                if 0 < set_x < self.objects_width and 0 < set_y < self.objects_height:
                    break
            configuration[ObjectType.Food].append((ObjectSubtype.FoodApple, set_x, set_y))


            #while True:
            #    new_x, new_y = random.randint(500, 155000) + 55, random.randint(500, 89500) + 55
            #    if  (colony1[0] - 15000 < new_x < colony1[0] + 15000 and colony1[1] - 12000 < new_y < colony1[1] + 12000) or \
            #        (colony2[0] - 15000 < new_x < colony2[0] + 15000 + 12000 and colony2[1] - 12000 < new_y < colony2[1] + 12000):
            #        continue
            #    configuration[ObjectType.Food].append((ObjectSubtype.FoodApple, random.randint(10000, 149900) + 55, random.randint(10000, 79000) + 55))
            #    break
        configuration[ObjectType.King1].append((ObjectSubtype.PlayerSimple, colony1[0] + 3000, colony1[1] + 3000))
        configuration[ObjectType.King2].append((ObjectSubtype.PlayerSimple, colony2[0] + 3000, colony2[1] + 3000))
        configuration[ObjectType.Colony1].append((ObjectSubtype.ColonySimple, colony1[0], colony1[1]))
        configuration[ObjectType.Colony2].append((ObjectSubtype.ColonySimple, colony2[0], colony2[1]))
        field_configuration = {"Ant": (np.int32(20), np.int32(3)),
                               "Food": (np.int32(20), np.int32(3)),
                               "Colony": (np.int32(25), np.int32(1))}
        self.messenger.objects_set_game_settings(self.objects_width, self.objects_height, self.field_width, self.field_height, configuration)
        self.messenger.field_set_field_settings(self.field_width, self.field_height, field_configuration)
        self.messenger.field_run_simulation()
        self.messenger.objects_run_simulation()
        self.renderer.set_game_settings(self.objects_width, self.objects_height, self.field_width, self.field_height)
        self.renderer.start_new_game()
        self.messenger.controls_start_game(self.objects_width, self.objects_height, self.field_width, self.field_height)

    def read_messages(self, dt):
        while True:
            self.data = self.messenger.get_message(messages.Game)
            if not self.data:
                break
            self.functions[self.data['func']](**self.data['args']) if 'args' in self.data else self.functions[self.data['func']]()

    def run_game(self):
        self.game_window = pyglet.window.Window(self.screen_width, self.screen_height)

        self.main_batch = pyglet.graphics.Batch()
        self.counter = pyglet.clock.ClockDisplay()

        self.game_state = GameState.ActiveGame


        @self.game_window.event
        def on_draw():
            self.fps_display.draw()

        @self.game_window.event
        def on_close():
            self.quit()

        @self.game_window.event
        def on_key_press(key, modif):
            self.messenger.controls_handle_key(True, key)

        @self.game_window.event
        def on_key_release(key, modif):
            self.messenger.controls_handle_key(False, key)

        @self.game_window.event
        def on_mouse_press(x, y, button, modifiers):
            bind = {mousebutton.RIGHT: MouseButton.Right,
                    mousebutton.LEFT: MouseButton.Left,
                    mousebutton.MIDDLE: MouseButton.Middle}
            mess = bind[button] if button in bind else None
            if mess is not None:
                self.messenger.controls_handle_mouse(mess, x, y)

        @self.game_window.event
        def on_mouse_scroll(x, y, scroll_x, scroll_y):
            if scroll_y > 0:
                self.messenger.controls_handle_mouse(MouseButton.ScrollUp, x, y)
            elif scroll_y < 0:
                self.messenger.controls_handle_mouse(MouseButton.ScrollDown, x, y)

        pyglet.clock.schedule_interval(self.read_messages, 1/30.0)
        pyglet.clock.schedule_interval(self.update_graphics, 1 / 60.0)

        pyglet.app.run()

game = PygletGame(1600, 900, False)
game.run_game()