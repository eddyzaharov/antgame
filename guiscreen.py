from multiprocessing import Process
from threading import Thread
import numpy as np
import pygame
import messages
from objects_template import *
from resource_manager import ResourceManager
from lib_obj import GameItem, GameItems

class GUIscreenState:
    Start, InMenu, InGame, Exit = range(4)

class GUIscreen(Thread):
    def __init__(self, messenger, screen_width, screen_height, frame_rate=60, verbose_fps=False):
        super(GUIscreen, self).__init__()
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.display_window = pygame.display.set_mode((screen_width, screen_height))

        self.rendered_surface = None
        self.cycles = 0
        self.verbose_fps = verbose_fps
        self.messenger = messenger

        self.frame_rate = frame_rate
        self.screen_state = GUIscreenState.Start
        self.clock = pygame.time.Clock()
        self.functions = {messages.GuiScreen.TerminateScreen: self.terminate_screen,
                          messages.GuiScreen.StartNewGame: self.start_new_game,
                          messages.GuiScreen.UpdateRenderedSurface: self.update_rendered_surface}

    def run(self):
        print("start screen in loop")
        while self.screen_state != GUIscreenState.Exit:
            while True:
                data = self.messenger.get_message(messages.GuiScreen)
                if not data:
                    break
                self.functions[data['func']](**data['args']) if 'args' in data else self.functions[data['func']]()
            if self.screen_state == GUIscreenState.InGame:
                self.update_game_representation()
                pygame.display.update()
            elapsed = self.clock.tick(self.frame_rate)
            if self.cycles % 30 == 0 and self.verbose_fps:
                print("screen elapsed: {}".format(elapsed))
            self.cycles += 1

    def terminate_screen(self):
        self.screen_state = GUIscreenState.Exit

    def update_game_representation(self):
        if self.rendered_surface is not None:
            self.display_window.blit(self.rendered_surface, (0, 0))

    def update_rendered_surface(self, surface_string):
        self.rendered_surface = pygame.image.frombuffer(surface_string, (self.screen_width, self.screen_height), 'RGB')

    def start_new_game(self):
        self.screen_state = GUIscreenState.InGame
