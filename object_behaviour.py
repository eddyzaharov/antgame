import numpy as np
from objects_template import *
from lib_obj import GameItems, GameItem


class Behaviour:
    def __init__(self,  messenger, kdtree, current_state, game_item_lib, game_items_lib):
        self.smell_field = None
        self.smell_item_field = None
        self.messenger = messenger
        self.game_item_lib = game_item_lib
        self.game_items_lib = game_items_lib
        self.objects_width = None
        self.objects_height = None
        self.kdtree = kdtree
        self.current_state = current_state
        self.nearest_objects = None
        self.current_object = None
        self.current_x_ind = None
        self.current_y_ind = None
        self.new_x_ind = None
        self.new_y_ind = None
        self.delta_x = None
        self.delta_y = None
        self.length = None
        self.x_norm = None
        self.y_norm = None
        self.dir_x = None
        self.dir_y = None
        self.obj_type= None
        self.field_smell = None
        self.grad_x = None
        self.grad_y = None
        self.x_coord = None
        self.y_coord = None
        self.left = None
        self.right = None
        self.down = None
        self.up = None
        self.bordering = None
        self.direction_x = None
        self.direction_y = None
        self.smell_type = None
        self.obj_type = None
        self.old_value = None
        self.new_props = None
        self.old_smell_type = None
        self.value = None
        self.koef = None
        self.new_x = None
        self.new_y = None
        self.x = None
        self.y = None
        self.obj_id = None
        self.obj_type = None
        self.obj_ids = None
        self.objids = None
        self.r = None
        self.nearest = None
        self.dest_x = None
        self.dest_y = None
        self.curr_x = None
        self.curr_y = None
        self.orient_type = None
        self.x_dir = None
        self.y_dir = None
        self.vel = None
        self.smell_value = None
        self.layer = None
        self.item_layers = None
        self.object_type, self.smell_cycles, self.colony_type, self.colony_x, self.colony_y = None, None, None, None, None
        self.colony, self.colony1, self.colony2, self.enemy_item, self.item_type = None, None, None, None, None
        self.friend_item, self.home_x, self.home_y, self.home_colony, self.home_colony_type = None, None, None, None, None
        self.enemy_x, self.enemy_y, self.enemy_colony, self.enemy_colony_type, self.enemy = None, None, None, None, None
        self.enemy_id, self.food, self.food_id, self.home, self.friend = None, None, None, None, None
        self.king_type, self.state, self.obj_subtype, self.state_time, self.energy = None, None, None, None, None
        self.carrying, self.dirvel, self.new_energy, self.new_state, self.enemy_obj = None, None, None, None, None
        self.statetime, self.nearest_search, self.search_x, self.search_y, self.search_obj_id = None, None, None, None, None
        self.search_obj_type, self.search_r, self.search_objids, self.search_orient_type = None, None, None, None

    def set_behaviour_settings(self, objects_width, objects_height, smell_field):
        self.objects_width = objects_width
        self.objects_height = objects_height
        self.smell_field = smell_field
        self.smell_item_field = np.zeros(smell_field.get_shape(), dtype=np.int32)

    def get_nearest_objects(self, r=None):
        self.search_x, self.search_y, self.search_obj_id, self.search_obj_type = self.game_item_lib.get_props((ObjectProp.Xcoord, ObjectProp.Ycoord, ObjectProp.ObjId, ObjectProp.ObjType), self.current_object)
        self.search_r = Constants.NearestRadius if r is None else r
        self.search_objids = self.kdtree.query_ball_point(self.search_x, self.search_y, self.search_r, self.search_obj_id)

        self.nearest_search = {}
        for item in self.search_objids:
            if item == self.search_obj_id:
                continue
            self.search_orient_type = InfoSmell.classificate(self.obj_type, ObjectType.type_by_id(item))
            if self.search_orient_type in self.nearest_search:
                self.nearest_search[self.search_orient_type].append(item)
            else:
                self.nearest_search[self.search_orient_type] = [item]
        return self.nearest_search

    def set_positioning_direction(self, x, y):
        self.game_item_lib.set_props({ObjectProp.Xdirection: x,
                                      ObjectProp.Ydirection: y,
                                      ObjectProp.DirVelocity: Constants.KingMaxSpeed}, self.current_object)

    def set_king_direction(self):
        if self.game_item_lib.destination_distance(self.current_object, False) < Constants.PlayerPositioningTreshold:
            self.game_item_lib.set_props({ObjectProp.DirVelocity: np.int32(0)}, self.current_object)
            return
        self.dest_x, self.dest_y, self.curr_x, self.curr_y = self.game_item_lib.get_props((ObjectProp.DestinationX, ObjectProp.DestinationY, ObjectProp.Xcoord, ObjectProp.Ycoord),
                                                                      self.current_object)
        if self.dest_x != np.int32(0) and self.dest_y != np.int32(0):
            self.delta_x, self.delta_y = self.dest_x - self.curr_x, self.dest_y - self.curr_y
            self.length = np.sqrt(np.power(self.delta_x, 2) + np.power(self.delta_y, 2))
            self.x_norm, self.y_norm = self.delta_x / self.length, self.delta_y / self.length
            if self.y_norm >= 0:
                # cos(22.5)
                if self.x_norm >= 0.92387953:
                    self.dir_x, self.dir_y = np.int32(1), np.int32(0)
                # cos(22.5+45)
                elif self.x_norm >= 0.38268343:
                    self.dir_x, self.dir_y = np.int32(1), np.int32(1)
                # cos(22.5+90)
                elif self.x_norm >= -0.38268343:
                    self.dir_x, self.dir_y = np.int32(0), np.int32(1)
                # cos(22.5+135)
                elif self.x_norm >= -0.92387953:
                    self.dir_x, self.dir_y = np.int32(-1), np.int32(1)
                else:
                    self.dir_x, self.dir_y = np.int32(-1), np.int32(0)
            else:
                if self.x_norm >= 0.92387953:
                    self.dir_x, self.dir_y = np.int32(1), np.int32(0)
                elif self.x_norm >= 0.38268343:
                    self.dir_x, self.dir_y = np.int32(1), np.int32(-1)
                elif self.x_norm >= -0.38268343:
                    self.dir_x, self.dir_y = np.int32(0), np.int32(-1)
                elif self.x_norm >= -0.92387953:
                    self.dir_x, self.dir_y = np.int32(-1), np.int32(-1)
                else:
                    self.dir_x, self.dir_y = np.int32(-1), np.int32(0)
            self.set_positioning_direction(self.dir_x, self.dir_y)

    def set_direction(self, smell, towards=True):
        if np.random.randint(0, 6) != 0:
            return
        self.obj_type, = self.game_item_lib.get_props((ObjectProp.ObjType,), self.current_object)
        self.field_smell = InfoSmell.convert_to_field_read(self.obj_type, smell)
        self.grad_x, self.grad_y = self.smell_field.get_gradient(self.current_x_ind, self.current_y_ind, self.field_smell, towards)
        if self.grad_x == 0 and self.grad_y == 0:
            self.x_coord, y_coord = self.game_item_lib.get_props((ObjectProp.Xcoord, ObjectProp.Ycoord), self.current_object)
            self.left, self.right, self.down, self.up = -1, 2, -1, 2
            self.bordering = False
            if self.x_coord < Constants.BorderAvoidTreshold:
                self.left = 0
                self.bordering = True
            elif self.x_coord > self.objects_width - Constants.BorderAvoidTreshold:
                self.right = 1
                self.bordering = True
            if self.y_coord < Constants.BorderAvoidTreshold:
                self.down = 0
                self.bordering = True
            elif self.y_coord > self.objects_height - Constants.BorderAvoidTreshold:
                self.up = 1
                self.bordering = True

            if self.bordering or np.random.randint(0, Constants.ChangeDirCooldown + 1) == 0:
            #    direction_x = 0
            #    direction_y = 0

            #    while direction_x == 0 and direction_y == 0:
            #        direction_x = np.random.randint(left, right)
            #        direction_y = np.random.randint(down, up)
                self.direction_x = np.random.randint(self.left, self.right)
                self.direction_y = np.random.randint(self.down, self.up)
            else:
                return
        else:
            self.direction_x, self.direction_y = self.grad_x, self.grad_y
        self.game_item_lib.set_props({ObjectProp.Xdirection: self.direction_x,
                                      ObjectProp.Ydirection: self.direction_y}, self.current_object)

    def update_all_state(self):
        self.smell_item_field.fill(0)
        for index in range(0, Constants.ObjArrayTotal):
            self.calc_new_state(index)
        self.smell_item_field.astype(np.int32)
        self.messenger.field_set_new_items_field(np.copy(self.smell_item_field))

    def delete_object(self):
        self.game_item_lib.wipe_object(self.current_object)

    def decrease_info_smell(self, value):
        self.smell_type, self.obj_type, self.old_value = self.game_item_lib.get_props((ObjectProp.InfoSmellType, ObjectProp.ObjType, ObjectProp.InfoSmellValue),
                                                                                       self.current_object)
        if self.obj_type == ObjectType.King1 or self.obj_type == ObjectType.King2:
            return
        if self.smell_type != InfoSmell.NoSmell:
            self.old_value = self.current_object[ObjectProp.InfoSmellValue]
            if value >= self.old_value:
                self.new_props = {ObjectProp.InfoSmellType: InfoSmell.NoSmell,
                                  ObjectProp.InfoSmellValue: np.int32(0)}
            else:
                self.new_props = {ObjectProp.InfoSmellValue: self.old_value - value}
            self.game_item_lib.set_props(self.new_props, self.current_object)

    def switch_info_smell(self, smell_type):
        self.old_smell_type, = self.game_item_lib.get_props((ObjectProp.InfoSmellType,), self.current_object)
        if self.old_smell_type == smell_type:
            return
        self.value = InfoSmell.start_value()
        self.game_item_lib.set_props({ObjectProp.InfoSmellCycles: np.int32(0),
                                      ObjectProp.InfoSmellType: smell_type,
                                      ObjectProp.InfoSmellValue: self.value}, self.current_object)

    def set_new_state(self, state):
        self.game_item_lib.set_props({ObjectProp.State: state,
                                      ObjectProp.StateTime: np.int32(0),
                                      ObjectProp.DirVelocity: np.int32(0)}, self.current_object)

    def set_speed(self, speed):
        self.game_item_lib.set_props({ObjectProp.DirVelocity: speed}, self.current_object)

    def set_energy(self, amount, increment=True):
        if increment:
            self.game_item_lib.increment_props({ObjectProp.Energy: amount}, self.current_object)
        else:
            self.game_item_lib.set_props({ObjectProp.Energy: amount}, self.current_object)

    def set_check_death(self):
        self.energy, self.state = self.game_item_lib.get_props((ObjectProp.Energy, ObjectProp.State), self.current_object)
        if self.energy <= np.int32(0) and self.state != ObjectState.ObjectDestroyed:
            self.game_item_lib.set_props({ObjectProp.DirVelocity: np.int32(0)}, self.current_object)
            self.set_new_state(ObjectState.ObjectDestroyed)
            return True
        return False

    def update_cycles(self):
        self.game_item_lib.increment_props({ObjectProp.StateTime: np.int32(1),
                                            ObjectProp.CyclesLiving: np.int32(1),
                                            ObjectProp.InfoSmellCycles: np.int32(1)}, self.current_object)

    def update_coordinates(self):
        self.x_coord, self.y_coord, self.x_dir, self.y_dir, self.vel = self.game_item_lib.get_props((ObjectProp.Xcoord, ObjectProp.Ycoord, ObjectProp.Xdirection,
                                                                            ObjectProp.Ydirection, ObjectProp.DirVelocity), self.current_object)
        self.koef = np.float(1.0) if self.x_dir * self.y_dir == np.int32(0) else np.float(1.41)
        self.new_x, self.new_y = self.x_coord + self.vel * self.x_dir // self.koef, self.y_coord + self.vel * self.y_dir // self.koef
        if self.new_x <= np.int32(0):
            self.new_x = np.int32(1)
        elif self.new_x >= self.objects_width:
            self.new_x = self.objects_width - 1
        if self.new_y <= np.int32(0):
            self.new_y = np.int32(0)
        elif self.new_y >= self.objects_height:
            self.new_y = np.int32(self.objects_height - 1)
        self.game_item_lib.set_props({ObjectProp.Xcoord: self.new_x,
                                      ObjectProp.Ycoord: self.new_y}, self.current_object)

    def fill_item_field(self):
        self.obj_type, self.smell_type, self.smell_value = self.game_item_lib.get_props((ObjectProp.ObjType, ObjectProp.InfoSmellType, ObjectProp.InfoSmellValue),
                                                                         self.current_object)
        if self.obj_type != ObjectType.Absent:
            self.layer = FieldSmell.get_smell(self.obj_type)
            self.smell_item_field[self.current_x_ind, self.current_y_ind, self.layer] = max(np.int32(1000), self.smell_item_field[self.current_x_ind,
                                                                                                                                  self.current_y_ind, self.layer])
            if self.smell_type == InfoSmell.NoSmell:
                return
            self.item_layers = InfoSmell.convert_to_field_smell_write(self.smell_type)
            for item_layer in self.item_layers:
                self.smell_item_field[self.current_x_ind, self.current_y_ind, item_layer] = max(self.smell_value,
                                                                                                self.smell_item_field[
                                                                                                    self.current_x_ind, self.current_y_ind, item_layer])

    def calc_new_state(self, index):
        self.current_object = self.current_state[index]
        self.x_coord, self.y_coord, self.object_type, self.smell_cycles = self.game_item_lib.get_props((ObjectProp.Xcoord, ObjectProp.Ycoord, ObjectProp.ObjType,
                                                                                                        ObjectProp.InfoSmellCycles,), self.current_object)
        if self.object_type == ObjectType.Absent:
            return
        self.current_x_ind, self.current_y_ind = self.smell_field.field_indexes(self.x_coord, self.y_coord)
        self.new_x_ind = None
        self.new_y_ind = None
        self.update_coordinates()

        if self.smell_cycles % InfoSmell.decay_rate() == np.int32(0):
            self.decrease_info_smell(InfoSmell.decay_value())

        self.nearest_objects = self.get_nearest_objects()

        if self.object_type == ObjectType.King1 or self.object_type == ObjectType.King2:
            self.king_behaviour()

        elif self.object_type == ObjectType.Ant1 or self.object_type == ObjectType.Ant2:
            self.ant_behaviour()

        elif self.object_type == ObjectType.Food:
            self.food_behaviour()

        elif self.object_type == ObjectType.Colony1 or self.object_type == ObjectType.Colony2:
            self.colony_behaviour()

        if self.object_type == ObjectType.Absent:
            return

        self.fill_item_field()
        self.update_cycles()
        return

    def king_behaviour(self):
        self.king_type, self.state, self.obj_subtype, self.state_time, self.energy, self.carrying, self.dirvel = self.game_item_lib.get_props((ObjectProp.ObjType, ObjectProp.State,
                                                                                                                                               ObjectProp.ObjSubtype, ObjectProp.StateTime,
                                                                                                                                               ObjectProp.Energy, ObjectProp.Carrying,
                                                                                                                                               ObjectProp.DirVelocity), self.current_object)
        if self.set_check_death():
            return
        self.enemy = InfoSmell.Ant2 if self.king_type == ObjectType.King1 else InfoSmell.Ant1
        self.friend = InfoSmell.Ant1 if self.king_type == ObjectType.King1 else InfoSmell.Ant2
        self.home = InfoSmell.Colony1 if self.king_type == ObjectType.King1 else InfoSmell.Colony2

        if self.state == ObjectState.KingFree:
            if self.dirvel == 0:
                if self.enemy in self.nearest_objects:
                    self.game_item_lib.set_props({ObjectProp.DirVelocity: np.int32(0)}, self.current_object)
                    self.set_new_state(ObjectState.KingAutoAttack)
                    return
                if self.home in self.nearest_objects:
                    if self.carrying > 0:
                        self.colony = self.game_items_lib.get_object_by_id(ObjectType.get_colony_id_by_type(self.king_type), self.current_state)
                        self.game_item_lib.increment_props({ObjectProp.Carrying: self.carrying}, self.colony)
                        self.game_item_lib.set_props({ObjectProp.Carrying: np.int32(0)}, self.current_object)
                    if self.energy < Constants.KingMaxHealth:
                        self.game_item_lib.set_props({ObjectProp.DirVelocity: np.int32(0)}, self.current_object)
                        self.set_new_state(ObjectState.Healing)
                        return
                if InfoSmell.Food in self.nearest_objects and self.carrying < 1:
                    self.game_item_lib.set_props({ObjectProp.DirVelocity: np.int32(0)}, self.current_object)
                    self.set_new_state(ObjectState.KingGatherFood)
                    return
            self.set_king_direction()

        elif self.state == ObjectState.Healing:
            if self.state_time < Constants.HealCooldown:
                return
            self.game_item_lib.set_props({ObjectProp.Energy: Constants.KingMaxHealth}, self.current_object)
            self.set_new_state(ObjectState.KingFree)

        elif self.state == ObjectState.KingGatherFood:
            if self.state_time < Constants.AttackCooldown:
                return
            if InfoSmell.Food in self.nearest_objects:
                self.food_id = self.nearest_objects[InfoSmell.Food][0]
                self.food = self.game_items_lib.get_object_by_id(self.food_id, self.current_state)
                self.game_item_lib.increment_props({ObjectProp.Energy: 3*np.int32(-Constants.AntFoodHit)}, self.food)
                self.game_item_lib.set_props({ObjectProp.Carrying: 3*Constants.AntCanCarry}, self.current_object)
            self.set_new_state(ObjectState.KingFree)

        elif self.state == ObjectState.KingAutoAttack:
            if self.state_time < Constants.AttackCooldown:
                return
            self.enemy_id = self.nearest_objects[self.enemy][0] if self.enemy in self.nearest_objects else None
            if self.enemy_id is not None:
                self.enemy = self.game_items_lib.get_object_by_id(self.enemy_id, self.current_state)
                self.game_item_lib.increment_props({ObjectProp.Energy: np.int32(-Constants.KingHitPower)}, self.enemy)
            self.set_new_state(ObjectState.KingFree)

        elif self.state == ObjectState.KingTeleporting:
            if self.state_time < Constants.CastLengthCycles:
                return
            self.nearest = self.get_nearest_objects(Constants.ActionRadius)
            self.enemy_colony_type = ObjectType.Colony2 if self.king_type == ObjectType.King1 else ObjectType.Colony1
            self.enemy_colony = self.game_items_lib.get_colony_by_object_type(self.enemy_colony_type, self.current_state)
            self.enemy_x, self.enemy_y = self.game_item_lib.get_props((ObjectProp.Xcoord, ObjectProp.Ycoord), self.enemy_colony)

            self.home_colony_type = ObjectType.Colony1 if self.king_type == ObjectType.King1 else ObjectType.Colony2
            self.home_colony = self.game_items_lib.get_colony_by_object_type(self.home_colony_type, self.current_state)
            self.home_x, self.home_y = self.game_item_lib.get_props((ObjectProp.Xcoord, ObjectProp.Ycoord), self.home_colony)
            if self.enemy in self.nearest:
                for enemy_item_id in self.nearest[self.enemy]:
                    self.enemy_item = self.game_items_lib.get_object_by_id(enemy_item_id, self.current_state)
                    self.item_type, = self.game_item_lib.get_props((ObjectProp.ObjType,), self.enemy_item)
                    if self.item_type in (ObjectType.Ant1, ObjectType.Ant2):
                        self.game_item_lib.set_props({ObjectProp.Xcoord: self.enemy_x + np.random.randint(-6000, 6000),
                                                      ObjectProp.Ycoord: self.enemy_y + np.random.randint(-6000, 6000),
                                                      ObjectProp.State: ObjectState.AntTeleported,
                                                      ObjectProp.StateTime: np.int32(0),
                                                      ObjectProp.DirVelocity: np.int32(0)}, self.enemy_item)
            if self.friend in self.nearest:
                for friend_item_id in self.nearest[self.friend]:
                    self.friend_item = self.game_items_lib.get_object_by_id(friend_item_id, self.current_state)
                    self.item_type, = self.game_item_lib.get_props((ObjectProp.ObjType,), self.friend_item)
                    if self.item_type in (ObjectType.Ant1, ObjectType.Ant2):
                        self.game_item_lib.set_props({ObjectProp.Xcoord: self.home_x + np.random.randint(-6000, 6000),
                                                      ObjectProp.Ycoord: self.home_y + np.random.randint(-6000, 6000),
                                                      ObjectProp.State: ObjectState.AntTeleported,
                                                      ObjectProp.StateTime: np.int32(0),
                                                      ObjectProp.DirVelocity: np.int32(0)}, self.friend_item)
            self.set_new_state(ObjectState.KingFree)

        elif self.state == ObjectState.KingBombing:
            print("king bombing")
            self.nearest = self.get_nearest_objects(Constants.ActionRadius)
            print(self.nearest, self.enemy)
            if self.enemy in self.nearest:
                print(self.nearest)
                for enemy_item_id in self.nearest[self.enemy]:
                    self.enemy_item = self.game_items_lib.get_object_by_id(enemy_item_id, self.current_state)
                    self.item_type, = self.game_item_lib.get_props((ObjectProp.ObjType,), self.enemy_item)
                    print(self.enemy, self.item_type, self.enemy_item)
                    if self.item_type in (ObjectType.Ant1, ObjectType.Ant2):
                        self.game_item_lib.set_props({ObjectProp.State: ObjectState.AntBombed,
                                                      ObjectProp.StateTime: np.int32(0),
                                                      ObjectProp.DirVelocity: np.int32(0)}, self.enemy_item)
                        print("inc props")
                        self.game_item_lib.increment_props({ObjectProp.Energy: -(Constants.BombHitPower + np.random.randint(-1000, 1000))}, self.enemy_item)
            self.set_new_state(ObjectState.KingFree)

        elif self.state == ObjectState.KingHealing:
            self.nearest = self.get_nearest_objects(Constants.ActionRadius)
            if self.friend in self.nearest:
                for friend_item_id in self.nearest[self.friend]:
                    self.friend_item = self.game_items_lib.get_object_by_id(friend_item_id, self.current_state)
                    self.item_type, = self.game_item_lib.get_props((ObjectProp.ObjType,), self.friend_item)
                    if self.item_type in (ObjectType.Ant1, ObjectType.Ant2):
                        self.game_item_lib.set_props({ObjectProp.State: ObjectState.Healing,
                                                      ObjectProp.StateTime: np.int32(0),
                                                      ObjectProp.DirVelocity: np.int32(0)}, self.friend_item)
            self.set_new_state(ObjectState.KingFree)

        elif self.state == ObjectState.KingFreezing:
            self.nearest = self.get_nearest_objects(Constants.ActionRadius)
            print("king freezing {}".format(self.nearest))
            if self.enemy in self.nearest:
                for enemy_item_id in self.nearest[self.enemy]:
                    self.enemy_item = self.game_items_lib.get_object_by_id(enemy_item_id, self.current_state)
                    self.item_type, = self.game_item_lib.get_props((ObjectProp.ObjType,), self.enemy_item)
                    if self.item_type in (ObjectType.Ant1, ObjectType.Ant2):
                        self.game_item_lib.set_props({ObjectProp.State: ObjectState.AntFreezed,
                                                      ObjectProp.StateTime: np.int32(0),
                                                      ObjectProp.DirVelocity: np.int32(0)}, self.enemy_item)
            self.set_new_state(ObjectState.KingFree)

        elif self.state == ObjectState.ObjectIdle:
            self.set_new_state(ObjectState.KingFree)

        elif self.state == ObjectState.ObjectDestroyed:
            if self.state_time < Constants.DissolveCycles:
                return
            self.colony1, self.colony2 = self.game_items_lib.get_colonies(self.current_state)
            if self.king_type == ObjectType.King1:
                self.colony = self.colony1
            else:
                self.colony = self.colony2
            self.colony_type, self.colony_x, self.colony_y = self.game_item_lib.get_props((ObjectProp.ObjType, ObjectProp.Xcoord, ObjectProp.Ycoord), self.colony)
            if self.colony_type == ObjectType.Absent:
                self.game_item_lib.set_props({ObjectProp.ObjType: ObjectType.Absent,
                                              ObjectProp.Energy: np.int32(0)}, self.current_object)
                return
            self.game_item_lib.set_props({ObjectProp.Xcoord: self.colony_x,
                                          ObjectProp.Ycoord: self.colony_y,
                                          ObjectProp.DestinationX: np.int32(0),
                                          ObjectProp.DestinationY: np.int32(0),
                                          ObjectProp.DirVelocity: np.int32(0),
                                          ObjectProp.Carrying: np.int32(0),
                                          ObjectProp.ActiveItem: np.int32(0),
                                          ObjectProp.Inventory: np.int32(0),
                                          ObjectProp.Energy: Constants.KingMaxHealth}, self.current_object)
            self.set_new_state(ObjectState.KingFree)
            return

    def ant_behaviour(self):
        if self.set_check_death():
            return

        self.obj_type, self.obj_subtype, self.state, self.x, self.y, \
        self.energy, vel, carrying, self.statetime = self.game_item_lib.get_props((ObjectProp.ObjType, ObjectProp.ObjSubtype, ObjectProp.State,
                                                                                   ObjectProp.Xcoord, ObjectProp.Ycoord,
                                                                                   ObjectProp.Energy, ObjectProp.DirVelocity, ObjectProp.Carrying,
                                                                                   ObjectProp.StateTime), self.current_object)
        self.enemy = InfoSmell.Ant2 if self.obj_type == ObjectType.Ant1 else InfoSmell.Ant1
        self.home = InfoSmell.Colony1 if self.obj_type == ObjectType.Ant1 else InfoSmell.Colony2
        if self.state == ObjectState.ObjectIdle:
            if self.obj_subtype == ObjectSubtype.AntWorker:
                self.new_state = ObjectState.AntSearchingFood if carrying < 1 else ObjectState.AntGoingHome
                self.set_new_state(self.new_state)
            else:
                self.set_new_state(ObjectState.AntSearchingEnemy)

        elif self.state == ObjectState.AntTeleported:
            if self.statetime < Constants.TeleportCooldownCycles:
                return
            self.set_new_state(ObjectState.ObjectIdle)

        elif self.state == ObjectState.AntBombed:
            if self.statetime < Constants.BombedCooldown:
                return
            self.set_new_state(ObjectState.ObjectIdle)

        elif self.state == ObjectState.AntFreezed:
            if self.statetime < Constants.FreezeLengthCycles:
                return
            self.set_new_state(ObjectState.ObjectIdle)

        elif self.state == ObjectState.AntSearchingEnemy:
            if self.energy < Constants.AntWarriorMaxEnergy // 2:
                self.set_new_state(ObjectState.AntGoingHome)
                return
            if self.enemy in self.nearest_objects:
                self.set_speed(np.int32(0))
                self.switch_info_smell(self.enemy)
                self.set_new_state(ObjectState.AntAttacking)
                return
            self.set_speed(Constants.AntMaxSpeed)
            self.set_direction(self.enemy)

        elif self.state == ObjectState.AntAttacking:
            if self.statetime < Constants.AttackCooldown:
                return
            self.enemy_id = self.nearest_objects[self.enemy][0] if self.enemy in self.nearest_objects else None
            if self.enemy_id is not None:
                self.enemy_obj = self.game_items_lib.get_object_by_id(self.enemy_id, self.current_state)
                self.game_item_lib.increment_props({ObjectProp.Energy: -Constants.AntHitPower}, self.enemy_obj)
            self.set_new_state(ObjectState.AntSearchingEnemy)

        elif self.state == ObjectState.AntSearchingFood:
            if self.energy < Constants.AntWarriorMaxEnergy // 2:
                self.set_new_state(ObjectState.AntGoingHome)
                return
            if self.enemy in self.nearest_objects:
                self.set_new_state(ObjectState.AntGoingHome)
            if InfoSmell.Food in self.nearest_objects:
                self.set_new_state(ObjectState.AntGathering)
                self.game_item_lib.set_props({ObjectProp.DirVelocity: np.int32(0)}, self.current_object)
                return
            self.set_speed(Constants.AntMaxSpeed)
            self.set_direction(InfoSmell.Food)

        elif self.state == ObjectState.AntGathering:
            if self.statetime < Constants.AttackCooldown:
                return
            if InfoSmell.Food in self.nearest_objects:
                self.food_id = self.nearest_objects[InfoSmell.Food][0]
                self.food = self.game_items_lib.get_object_by_id(self.food_id, self.current_state)
                self.game_item_lib.increment_props({ObjectProp.Energy: np.int32(-Constants.AntFoodHit)}, self.food)
                self.game_item_lib.set_props({ObjectProp.Carrying: Constants.AntCanCarry}, self.current_object)
                self.set_new_state(ObjectState.AntGoingHome)
                self.switch_info_smell(InfoSmell.Food)
            else:
                self.set_new_state(ObjectState.AntSearchingFood)
            return

        elif self.state == ObjectState.AntGoingHome:
            if self.home in self.nearest_objects:
                if self.obj_subtype == ObjectSubtype.AntWorker:
                    self.colony = self.game_items_lib.get_object_by_id(ObjectType.get_colony_id_by_type(self.obj_type), self.current_state)
                    self.game_item_lib.increment_props({ObjectProp.Carrying: carrying}, self.colony)
                    self.game_item_lib.set_props({ObjectProp.Carrying: np.int32(0)}, self.current_object)
                    if self.energy < Constants.AntWorkerMaxEnergy:
                        self.game_item_lib.set_props({ObjectProp.DirVelocity: np.int32(0)}, self.current_object)
                        self.set_new_state(ObjectState.Healing)
                        return
                    self.set_new_state(ObjectState.AntSearchingFood)
                else:
                    if self.energy < Constants.AntWorkerMaxEnergy:
                        self.game_item_lib.set_props({ObjectProp.DirVelocity: np.int32(0)}, self.current_object)
                        self.set_new_state(ObjectState.Healing)
                        return
                    self.set_new_state(ObjectState.AntSearchingEnemy)
                self.switch_info_smell(self.home)
            self.set_speed(Constants.AntMaxSpeed)
            self.set_direction(self.home)

        elif self.state == ObjectState.Healing:
            if self.statetime < Constants.HealCooldown:
                return
            if self.obj_subtype == ObjectSubtype.AntWorker:
                self.new_energy = Constants.AntWorkerMaxEnergy
            else:
                self.new_energy = Constants.AntWarriorMaxEnergy
            self.game_item_lib.set_props({ObjectProp.Energy: self.new_energy}, self.current_object)
            self.set_new_state(ObjectState.ObjectIdle)

        elif self.state == ObjectState.AntTeleported:
            if self.statetime < Constants.TeleportCooldownCycles:
                return
            self.set_new_state(ObjectState.ObjectIdle)

        elif self.state == ObjectState.ObjectDestroyed:
            if self.statetime > Constants.DissolveCycles:
                self.messenger.objects_add_object(ObjectType.Food, ObjectSubtype.FoodCorpse, self.x, self.y)
                self.delete_object()
                return

    def colony_behaviour(self):
        if self.set_check_death():
            return
        self.state, self.statetime = self.game_item_lib.get_props((ObjectProp.State, ObjectProp.StateTime), self.current_object)
        if self.state == ObjectState.ObjectIdle:
            pass
        elif self.state == ObjectState.ObjectDestroyed:
            if self.statetime > Constants.DissolveCycles:
                self.delete_object()

    def food_behaviour(self):
        if self.set_check_death():
            return
        self.state, self.statetime = self.game_item_lib.get_props((ObjectProp.State, ObjectProp.StateTime), self.current_object)
        if self.state == ObjectState.ObjectIdle:
            pass
        elif self.state == ObjectState.ObjectDestroyed:
            if self.statetime > Constants.DissolveCycles:
                self.delete_object()
                return
